# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'mainwindow.ui'
##
## Created by: Qt User Interface Compiler version 5.15.0
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import (QCoreApplication, QDate, QDateTime, QMetaObject,
    QObject, QPoint, QRect, QSize, QTime, QUrl, Qt)
from PySide2.QtGui import (QBrush, QColor, QConicalGradient, QCursor, QFont,
    QFontDatabase, QIcon, QKeySequence, QLinearGradient, QPalette, QPainter,
    QPixmap, QRadialGradient)
from PySide2.QtWidgets import *


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        if not MainWindow.objectName():
            MainWindow.setObjectName(u"MainWindow")
        MainWindow.resize(479, 480)
        sizePolicy = QSizePolicy(QSizePolicy.Minimum, QSizePolicy.Minimum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(MainWindow.sizePolicy().hasHeightForWidth())
        MainWindow.setSizePolicy(sizePolicy)
        palette = QPalette()
        brush = QBrush(QColor(0, 0, 0, 255))
        brush.setStyle(Qt.SolidPattern)
        palette.setBrush(QPalette.Active, QPalette.WindowText, brush)
        brush1 = QBrush(QColor(255, 255, 255, 255))
        brush1.setStyle(Qt.SolidPattern)
        palette.setBrush(QPalette.Active, QPalette.Button, brush1)
        palette.setBrush(QPalette.Active, QPalette.Light, brush1)
        brush2 = QBrush(QColor(246, 246, 245, 255))
        brush2.setStyle(Qt.SolidPattern)
        palette.setBrush(QPalette.Active, QPalette.Midlight, brush2)
        brush3 = QBrush(QColor(119, 119, 118, 255))
        brush3.setStyle(Qt.SolidPattern)
        palette.setBrush(QPalette.Active, QPalette.Dark, brush3)
        brush4 = QBrush(QColor(159, 159, 157, 255))
        brush4.setStyle(Qt.SolidPattern)
        palette.setBrush(QPalette.Active, QPalette.Mid, brush4)
        palette.setBrush(QPalette.Active, QPalette.Text, brush)
        palette.setBrush(QPalette.Active, QPalette.BrightText, brush1)
        palette.setBrush(QPalette.Active, QPalette.ButtonText, brush)
        palette.setBrush(QPalette.Active, QPalette.Base, brush1)
        palette.setBrush(QPalette.Active, QPalette.Window, brush1)
        palette.setBrush(QPalette.Active, QPalette.Shadow, brush)
        palette.setBrush(QPalette.Active, QPalette.AlternateBase, brush2)
        brush5 = QBrush(QColor(255, 255, 220, 255))
        brush5.setStyle(Qt.SolidPattern)
        palette.setBrush(QPalette.Active, QPalette.ToolTipBase, brush5)
        palette.setBrush(QPalette.Active, QPalette.ToolTipText, brush)
        palette.setBrush(QPalette.Inactive, QPalette.WindowText, brush)
        palette.setBrush(QPalette.Inactive, QPalette.Button, brush1)
        palette.setBrush(QPalette.Inactive, QPalette.Light, brush1)
        palette.setBrush(QPalette.Inactive, QPalette.Midlight, brush2)
        palette.setBrush(QPalette.Inactive, QPalette.Dark, brush3)
        palette.setBrush(QPalette.Inactive, QPalette.Mid, brush4)
        palette.setBrush(QPalette.Inactive, QPalette.Text, brush)
        palette.setBrush(QPalette.Inactive, QPalette.BrightText, brush1)
        palette.setBrush(QPalette.Inactive, QPalette.ButtonText, brush)
        palette.setBrush(QPalette.Inactive, QPalette.Base, brush1)
        palette.setBrush(QPalette.Inactive, QPalette.Window, brush1)
        palette.setBrush(QPalette.Inactive, QPalette.Shadow, brush)
        palette.setBrush(QPalette.Inactive, QPalette.AlternateBase, brush2)
        palette.setBrush(QPalette.Inactive, QPalette.ToolTipBase, brush5)
        palette.setBrush(QPalette.Inactive, QPalette.ToolTipText, brush)
        palette.setBrush(QPalette.Disabled, QPalette.WindowText, brush3)
        palette.setBrush(QPalette.Disabled, QPalette.Button, brush1)
        palette.setBrush(QPalette.Disabled, QPalette.Light, brush1)
        palette.setBrush(QPalette.Disabled, QPalette.Midlight, brush2)
        palette.setBrush(QPalette.Disabled, QPalette.Dark, brush3)
        palette.setBrush(QPalette.Disabled, QPalette.Mid, brush4)
        palette.setBrush(QPalette.Disabled, QPalette.Text, brush3)
        palette.setBrush(QPalette.Disabled, QPalette.BrightText, brush1)
        palette.setBrush(QPalette.Disabled, QPalette.ButtonText, brush3)
        palette.setBrush(QPalette.Disabled, QPalette.Base, brush1)
        palette.setBrush(QPalette.Disabled, QPalette.Window, brush1)
        palette.setBrush(QPalette.Disabled, QPalette.Shadow, brush)
        brush6 = QBrush(QColor(238, 238, 236, 255))
        brush6.setStyle(Qt.SolidPattern)
        palette.setBrush(QPalette.Disabled, QPalette.AlternateBase, brush6)
        palette.setBrush(QPalette.Disabled, QPalette.ToolTipBase, brush5)
        palette.setBrush(QPalette.Disabled, QPalette.ToolTipText, brush)
        MainWindow.setPalette(palette)
        MainWindow.setStyleSheet(u"background-color: rgb(255, 255, 255);")
        MainWindow.setTabShape(QTabWidget.Rounded)
        self.centralWidget = QWidget(MainWindow)
        self.centralWidget.setObjectName(u"centralWidget")
        self.centralWidget.setStyleSheet(u"background-color: rgb(255, 255, 255);")
        self.verticalLayout_30 = QVBoxLayout(self.centralWidget)
        self.verticalLayout_30.setSpacing(6)
        self.verticalLayout_30.setContentsMargins(11, 11, 11, 11)
        self.verticalLayout_30.setObjectName(u"verticalLayout_30")
        self.general_tabs = QTabWidget(self.centralWidget)
        self.general_tabs.setObjectName(u"general_tabs")
        self.User = QWidget()
        self.User.setObjectName(u"User")
        self.User.setCursor(QCursor(Qt.ArrowCursor))
        self.verticalLayout = QVBoxLayout(self.User)
        self.verticalLayout.setSpacing(6)
        self.verticalLayout.setContentsMargins(11, 11, 11, 11)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.user_tabs = QStackedWidget(self.User)
        self.user_tabs.setObjectName(u"user_tabs")
        self.user_products_page = QWidget()
        self.user_products_page.setObjectName(u"user_products_page")
        self.gridLayout = QGridLayout(self.user_products_page)
        self.gridLayout.setSpacing(6)
        self.gridLayout.setContentsMargins(11, 11, 11, 11)
        self.gridLayout.setObjectName(u"gridLayout")
        self.user_p_16 = QPushButton(self.user_products_page)
        self.user_p_16.setObjectName(u"user_p_16")
        sizePolicy1 = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Preferred)
        sizePolicy1.setHorizontalStretch(0)
        sizePolicy1.setVerticalStretch(0)
        sizePolicy1.setHeightForWidth(self.user_p_16.sizePolicy().hasHeightForWidth())
        self.user_p_16.setSizePolicy(sizePolicy1)
        self.user_p_16.setStyleSheet(u"border-image: url(Images/image16.jpg);")

        self.gridLayout.addWidget(self.user_p_16, 4, 2, 1, 1)

        self.user_p_13 = QPushButton(self.user_products_page)
        self.user_p_13.setObjectName(u"user_p_13")
        sizePolicy1.setHeightForWidth(self.user_p_13.sizePolicy().hasHeightForWidth())
        self.user_p_13.setSizePolicy(sizePolicy1)
        self.user_p_13.setStyleSheet(u"border-image: url(Images/image13.jpg);")

        self.gridLayout.addWidget(self.user_p_13, 3, 4, 1, 1)

        self.user_p_9 = QPushButton(self.user_products_page)
        self.user_p_9.setObjectName(u"user_p_9")
        sizePolicy1.setHeightForWidth(self.user_p_9.sizePolicy().hasHeightForWidth())
        self.user_p_9.setSizePolicy(sizePolicy1)
        self.user_p_9.setStyleSheet(u"border-image: url(Images/image9.jpg);")

        self.gridLayout.addWidget(self.user_p_9, 2, 5, 1, 1)

        self.user_p_2 = QPushButton(self.user_products_page)
        self.user_p_2.setObjectName(u"user_p_2")
        sizePolicy1.setHeightForWidth(self.user_p_2.sizePolicy().hasHeightForWidth())
        self.user_p_2.setSizePolicy(sizePolicy1)
        self.user_p_2.setStyleSheet(u"border-image: url(Images/image2.jpg);")

        self.gridLayout.addWidget(self.user_p_2, 1, 3, 1, 1)

        self.user_p_21 = QPushButton(self.user_products_page)
        self.user_p_21.setObjectName(u"user_p_21")
        sizePolicy1.setHeightForWidth(self.user_p_21.sizePolicy().hasHeightForWidth())
        self.user_p_21.setSizePolicy(sizePolicy1)
        self.user_p_21.setStyleSheet(u"border-image: url(Images/image21.jpg);")

        self.gridLayout.addWidget(self.user_p_21, 5, 2, 1, 1)

        self.user_p_15 = QPushButton(self.user_products_page)
        self.user_p_15.setObjectName(u"user_p_15")
        sizePolicy1.setHeightForWidth(self.user_p_15.sizePolicy().hasHeightForWidth())
        self.user_p_15.setSizePolicy(sizePolicy1)
        self.user_p_15.setStyleSheet(u"border-image: url(Images/image15.jpg);")

        self.gridLayout.addWidget(self.user_p_15, 4, 1, 1, 1)

        self.user_p_3 = QPushButton(self.user_products_page)
        self.user_p_3.setObjectName(u"user_p_3")
        sizePolicy1.setHeightForWidth(self.user_p_3.sizePolicy().hasHeightForWidth())
        self.user_p_3.setSizePolicy(sizePolicy1)
        self.user_p_3.setStyleSheet(u"border-image: url(Images/image3.jpg);")

        self.gridLayout.addWidget(self.user_p_3, 1, 4, 1, 1)

        self.user_p_19 = QPushButton(self.user_products_page)
        self.user_p_19.setObjectName(u"user_p_19")
        sizePolicy1.setHeightForWidth(self.user_p_19.sizePolicy().hasHeightForWidth())
        self.user_p_19.setSizePolicy(sizePolicy1)
        self.user_p_19.setStyleSheet(u"border-image: url(Images/image19.jpg);")

        self.gridLayout.addWidget(self.user_p_19, 4, 5, 1, 1)

        self.user_p_0 = QPushButton(self.user_products_page)
        self.user_p_0.setObjectName(u"user_p_0")
        sizePolicy1.setHeightForWidth(self.user_p_0.sizePolicy().hasHeightForWidth())
        self.user_p_0.setSizePolicy(sizePolicy1)
        self.user_p_0.setAutoFillBackground(False)
        self.user_p_0.setStyleSheet(u"border-image: url(Images/image0.jpg);")
        self.user_p_0.setAutoDefault(False)
        self.user_p_0.setFlat(False)

        self.gridLayout.addWidget(self.user_p_0, 1, 1, 1, 1)

        self.user_p_20 = QPushButton(self.user_products_page)
        self.user_p_20.setObjectName(u"user_p_20")
        sizePolicy1.setHeightForWidth(self.user_p_20.sizePolicy().hasHeightForWidth())
        self.user_p_20.setSizePolicy(sizePolicy1)
        self.user_p_20.setStyleSheet(u"border-image: url(Images/image20.jpg);")

        self.gridLayout.addWidget(self.user_p_20, 5, 1, 1, 1)

        self.user_p_14 = QPushButton(self.user_products_page)
        self.user_p_14.setObjectName(u"user_p_14")
        sizePolicy1.setHeightForWidth(self.user_p_14.sizePolicy().hasHeightForWidth())
        self.user_p_14.setSizePolicy(sizePolicy1)
        self.user_p_14.setStyleSheet(u"border-image: url(Images/image14.jpg);")

        self.gridLayout.addWidget(self.user_p_14, 3, 5, 1, 1)

        self.user_p_4 = QPushButton(self.user_products_page)
        self.user_p_4.setObjectName(u"user_p_4")
        sizePolicy1.setHeightForWidth(self.user_p_4.sizePolicy().hasHeightForWidth())
        self.user_p_4.setSizePolicy(sizePolicy1)
        self.user_p_4.setStyleSheet(u"border-image: url(Images/image4.jpg);")

        self.gridLayout.addWidget(self.user_p_4, 1, 5, 1, 1)

        self.user_p_17 = QPushButton(self.user_products_page)
        self.user_p_17.setObjectName(u"user_p_17")
        sizePolicy1.setHeightForWidth(self.user_p_17.sizePolicy().hasHeightForWidth())
        self.user_p_17.setSizePolicy(sizePolicy1)
        self.user_p_17.setStyleSheet(u"border-image: url(Images/image17.jpg);")

        self.gridLayout.addWidget(self.user_p_17, 4, 3, 1, 1)

        self.user_p_10 = QPushButton(self.user_products_page)
        self.user_p_10.setObjectName(u"user_p_10")
        sizePolicy1.setHeightForWidth(self.user_p_10.sizePolicy().hasHeightForWidth())
        self.user_p_10.setSizePolicy(sizePolicy1)
        self.user_p_10.setStyleSheet(u"border-image: url(Images/image10.jpg);")

        self.gridLayout.addWidget(self.user_p_10, 3, 1, 1, 1)

        self.user_p_12 = QPushButton(self.user_products_page)
        self.user_p_12.setObjectName(u"user_p_12")
        sizePolicy1.setHeightForWidth(self.user_p_12.sizePolicy().hasHeightForWidth())
        self.user_p_12.setSizePolicy(sizePolicy1)
        self.user_p_12.setStyleSheet(u"border-image: url(Images/image12.jpg);")

        self.gridLayout.addWidget(self.user_p_12, 3, 3, 1, 1)

        self.user_p_7 = QPushButton(self.user_products_page)
        self.user_p_7.setObjectName(u"user_p_7")
        sizePolicy1.setHeightForWidth(self.user_p_7.sizePolicy().hasHeightForWidth())
        self.user_p_7.setSizePolicy(sizePolicy1)
        self.user_p_7.setStyleSheet(u"border-image: url(Images/image7.jpg);")

        self.gridLayout.addWidget(self.user_p_7, 2, 3, 1, 1)

        self.user_p_23 = QPushButton(self.user_products_page)
        self.user_p_23.setObjectName(u"user_p_23")
        sizePolicy1.setHeightForWidth(self.user_p_23.sizePolicy().hasHeightForWidth())
        self.user_p_23.setSizePolicy(sizePolicy1)
        self.user_p_23.setStyleSheet(u"border-image: url(Images/image23.jpg);")

        self.gridLayout.addWidget(self.user_p_23, 5, 4, 1, 1)

        self.user_p_8 = QPushButton(self.user_products_page)
        self.user_p_8.setObjectName(u"user_p_8")
        sizePolicy1.setHeightForWidth(self.user_p_8.sizePolicy().hasHeightForWidth())
        self.user_p_8.setSizePolicy(sizePolicy1)
        self.user_p_8.setStyleSheet(u"border-image: url(Images/image8.jpg);")

        self.gridLayout.addWidget(self.user_p_8, 2, 4, 1, 1)

        self.user_p_11 = QPushButton(self.user_products_page)
        self.user_p_11.setObjectName(u"user_p_11")
        sizePolicy1.setHeightForWidth(self.user_p_11.sizePolicy().hasHeightForWidth())
        self.user_p_11.setSizePolicy(sizePolicy1)
        self.user_p_11.setStyleSheet(u"border-image: url(Images/image11.jpg);")

        self.gridLayout.addWidget(self.user_p_11, 3, 2, 1, 1)

        self.user_p_5 = QPushButton(self.user_products_page)
        self.user_p_5.setObjectName(u"user_p_5")
        sizePolicy1.setHeightForWidth(self.user_p_5.sizePolicy().hasHeightForWidth())
        self.user_p_5.setSizePolicy(sizePolicy1)
        self.user_p_5.setStyleSheet(u"border-image: url(Images/image5.jpg);")

        self.gridLayout.addWidget(self.user_p_5, 2, 1, 1, 1)

        self.user_p_24 = QPushButton(self.user_products_page)
        self.user_p_24.setObjectName(u"user_p_24")
        sizePolicy1.setHeightForWidth(self.user_p_24.sizePolicy().hasHeightForWidth())
        self.user_p_24.setSizePolicy(sizePolicy1)
        self.user_p_24.setStyleSheet(u"border-image: url(Images/image24.jpg);")

        self.gridLayout.addWidget(self.user_p_24, 5, 5, 1, 1)

        self.user_p_1 = QPushButton(self.user_products_page)
        self.user_p_1.setObjectName(u"user_p_1")
        sizePolicy1.setHeightForWidth(self.user_p_1.sizePolicy().hasHeightForWidth())
        self.user_p_1.setSizePolicy(sizePolicy1)
        self.user_p_1.setStyleSheet(u"border-image: url(Images/image1.jpg);")

        self.gridLayout.addWidget(self.user_p_1, 1, 2, 1, 1)

        self.user_p_18 = QPushButton(self.user_products_page)
        self.user_p_18.setObjectName(u"user_p_18")
        sizePolicy1.setHeightForWidth(self.user_p_18.sizePolicy().hasHeightForWidth())
        self.user_p_18.setSizePolicy(sizePolicy1)
        self.user_p_18.setStyleSheet(u"border-image: url(Images/image18.jpg);")

        self.gridLayout.addWidget(self.user_p_18, 4, 4, 1, 1)

        self.user_p_6 = QPushButton(self.user_products_page)
        self.user_p_6.setObjectName(u"user_p_6")
        sizePolicy1.setHeightForWidth(self.user_p_6.sizePolicy().hasHeightForWidth())
        self.user_p_6.setSizePolicy(sizePolicy1)
        self.user_p_6.setStyleSheet(u"border-image: url(Images/image6.jpg);")

        self.gridLayout.addWidget(self.user_p_6, 2, 2, 1, 1)

        self.user_p_22 = QPushButton(self.user_products_page)
        self.user_p_22.setObjectName(u"user_p_22")
        sizePolicy1.setHeightForWidth(self.user_p_22.sizePolicy().hasHeightForWidth())
        self.user_p_22.setSizePolicy(sizePolicy1)
        self.user_p_22.setStyleSheet(u"border-image: url(Images/image22.jpg);")

        self.gridLayout.addWidget(self.user_p_22, 5, 3, 1, 1)

        self.user_tabs.addWidget(self.user_products_page)
        self.user_shopping_page = QWidget()
        self.user_shopping_page.setObjectName(u"user_shopping_page")
        self.gridLayout_4 = QGridLayout(self.user_shopping_page)
        self.gridLayout_4.setSpacing(6)
        self.gridLayout_4.setContentsMargins(11, 11, 11, 11)
        self.gridLayout_4.setObjectName(u"gridLayout_4")
        self.user_image_label = QLabel(self.user_shopping_page)
        self.user_image_label.setObjectName(u"user_image_label")
        self.user_image_label.setTextFormat(Qt.RichText)

        self.gridLayout_4.addWidget(self.user_image_label, 1, 1, 1, 1)

        self.user_price_label = QLabel(self.user_shopping_page)
        self.user_price_label.setObjectName(u"user_price_label")
        sizePolicy2 = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Minimum)
        sizePolicy2.setHorizontalStretch(0)
        sizePolicy2.setVerticalStretch(0)
        sizePolicy2.setHeightForWidth(self.user_price_label.sizePolicy().hasHeightForWidth())
        self.user_price_label.setSizePolicy(sizePolicy2)
        self.user_price_label.setAlignment(Qt.AlignCenter)

        self.gridLayout_4.addWidget(self.user_price_label, 1, 2, 1, 1)

        self.user_back_button = QPushButton(self.user_shopping_page)
        self.user_back_button.setObjectName(u"user_back_button")
        self.user_back_button.setStyleSheet(u"color: rgb(255, 255, 255);\n"
"background-color: rgb(32, 74, 135);")

        self.gridLayout_4.addWidget(self.user_back_button, 0, 1, 1, 1)

        self.user_buy_button = QPushButton(self.user_shopping_page)
        self.user_buy_button.setObjectName(u"user_buy_button")
        self.user_buy_button.setStyleSheet(u"color: rgb(255, 255, 255);\n"
"background-color: rgb(32, 74, 135);")

        self.gridLayout_4.addWidget(self.user_buy_button, 2, 1, 1, 1)

        self.user_name_label = QLabel(self.user_shopping_page)
        self.user_name_label.setObjectName(u"user_name_label")

        self.gridLayout_4.addWidget(self.user_name_label, 1, 0, 1, 1)

        self.user_tabs.addWidget(self.user_shopping_page)
        self.user_thanks_page = QWidget()
        self.user_thanks_page.setObjectName(u"user_thanks_page")
        self.verticalLayout_2 = QVBoxLayout(self.user_thanks_page)
        self.verticalLayout_2.setSpacing(6)
        self.verticalLayout_2.setContentsMargins(11, 11, 11, 11)
        self.verticalLayout_2.setObjectName(u"verticalLayout_2")
        self.label = QLabel(self.user_thanks_page)
        self.label.setObjectName(u"label")

        self.verticalLayout_2.addWidget(self.label)

        self.user_tabs.addWidget(self.user_thanks_page)

        self.verticalLayout.addWidget(self.user_tabs)

        self.general_tabs.addTab(self.User, "")
        self.Admin = QWidget()
        self.Admin.setObjectName(u"Admin")
        self.verticalLayout_3 = QVBoxLayout(self.Admin)
        self.verticalLayout_3.setSpacing(6)
        self.verticalLayout_3.setContentsMargins(11, 11, 11, 11)
        self.verticalLayout_3.setObjectName(u"verticalLayout_3")
        self.admin_tabs = QStackedWidget(self.Admin)
        self.admin_tabs.setObjectName(u"admin_tabs")
        self.admin_password_page = QWidget()
        self.admin_password_page.setObjectName(u"admin_password_page")
        self.gridLayout_2 = QGridLayout(self.admin_password_page)
        self.gridLayout_2.setSpacing(6)
        self.gridLayout_2.setContentsMargins(11, 11, 11, 11)
        self.gridLayout_2.setObjectName(u"gridLayout_2")
        self.d3 = QLabel(self.admin_password_page)
        self.d3.setObjectName(u"d3")
        sizePolicy3 = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Preferred)
        sizePolicy3.setHorizontalStretch(0)
        sizePolicy3.setVerticalStretch(0)
        sizePolicy3.setHeightForWidth(self.d3.sizePolicy().hasHeightForWidth())
        self.d3.setSizePolicy(sizePolicy3)

        self.gridLayout_2.addWidget(self.d3, 1, 0, 1, 1)

        self.d4 = QLabel(self.admin_password_page)
        self.d4.setObjectName(u"d4")
        sizePolicy3.setHeightForWidth(self.d4.sizePolicy().hasHeightForWidth())
        self.d4.setSizePolicy(sizePolicy3)

        self.gridLayout_2.addWidget(self.d4, 1, 2, 1, 1)

        self.d5 = QLabel(self.admin_password_page)
        self.d5.setObjectName(u"d5")
        sizePolicy3.setHeightForWidth(self.d5.sizePolicy().hasHeightForWidth())
        self.d5.setSizePolicy(sizePolicy3)

        self.gridLayout_2.addWidget(self.d5, 2, 0, 1, 1)

        self.admin_password_text = QLineEdit(self.admin_password_page)
        self.admin_password_text.setObjectName(u"admin_password_text")
        self.admin_password_text.setEchoMode(QLineEdit.Password)

        self.gridLayout_2.addWidget(self.admin_password_text, 0, 1, 1, 1)

        self.admin_password_button = QPushButton(self.admin_password_page)
        self.admin_password_button.setObjectName(u"admin_password_button")
        self.admin_password_button.setStyleSheet(u"background-color: rgb(32, 74, 135);\n"
"color: rgb(255, 255, 255);")

        self.gridLayout_2.addWidget(self.admin_password_button, 1, 1, 1, 1)

        self.d2 = QLabel(self.admin_password_page)
        self.d2.setObjectName(u"d2")
        sizePolicy3.setHeightForWidth(self.d2.sizePolicy().hasHeightForWidth())
        self.d2.setSizePolicy(sizePolicy3)

        self.gridLayout_2.addWidget(self.d2, 0, 2, 1, 1)

        self.d6 = QLabel(self.admin_password_page)
        self.d6.setObjectName(u"d6")
        sizePolicy3.setHeightForWidth(self.d6.sizePolicy().hasHeightForWidth())
        self.d6.setSizePolicy(sizePolicy3)

        self.gridLayout_2.addWidget(self.d6, 2, 2, 1, 1)

        self.admin_error_message = QLabel(self.admin_password_page)
        self.admin_error_message.setObjectName(u"admin_error_message")
        self.admin_error_message.setStyleSheet(u"color: rgb(239, 41, 41);")
        self.admin_error_message.setAlignment(Qt.AlignCenter)

        self.gridLayout_2.addWidget(self.admin_error_message, 2, 1, 1, 1)

        self.d1 = QLabel(self.admin_password_page)
        self.d1.setObjectName(u"d1")
        sizePolicy3.setHeightForWidth(self.d1.sizePolicy().hasHeightForWidth())
        self.d1.setSizePolicy(sizePolicy3)
        self.d1.setAlignment(Qt.AlignCenter)

        self.gridLayout_2.addWidget(self.d1, 0, 0, 1, 1)

        self.admin_tabs.addWidget(self.admin_password_page)
        self.admin_products_page = QWidget()
        self.admin_products_page.setObjectName(u"admin_products_page")
        self.gridLayout_3 = QGridLayout(self.admin_products_page)
        self.gridLayout_3.setSpacing(6)
        self.gridLayout_3.setContentsMargins(11, 11, 11, 11)
        self.gridLayout_3.setObjectName(u"gridLayout_3")
        self.admin_f_14 = QFrame(self.admin_products_page)
        self.admin_f_14.setObjectName(u"admin_f_14")
        self.admin_f_14.setFrameShape(QFrame.StyledPanel)
        self.admin_f_14.setFrameShadow(QFrame.Raised)
        self.verticalLayout_19 = QVBoxLayout(self.admin_f_14)
        self.verticalLayout_19.setSpacing(6)
        self.verticalLayout_19.setContentsMargins(11, 11, 11, 11)
        self.verticalLayout_19.setObjectName(u"verticalLayout_19")
        self.admin_p_14 = QPushButton(self.admin_f_14)
        self.admin_p_14.setObjectName(u"admin_p_14")
        sizePolicy1.setHeightForWidth(self.admin_p_14.sizePolicy().hasHeightForWidth())
        self.admin_p_14.setSizePolicy(sizePolicy1)
        self.admin_p_14.setStyleSheet(u"border-image: url(Images/image14.jpg);")

        self.verticalLayout_19.addWidget(self.admin_p_14)


        self.gridLayout_3.addWidget(self.admin_f_14, 2, 6, 1, 1)

        self.admin_f_23 = QFrame(self.admin_products_page)
        self.admin_f_23.setObjectName(u"admin_f_23")
        self.admin_f_23.setFrameShape(QFrame.StyledPanel)
        self.admin_f_23.setFrameShadow(QFrame.Raised)
        self.verticalLayout_28 = QVBoxLayout(self.admin_f_23)
        self.verticalLayout_28.setSpacing(6)
        self.verticalLayout_28.setContentsMargins(11, 11, 11, 11)
        self.verticalLayout_28.setObjectName(u"verticalLayout_28")
        self.admin_p_23 = QPushButton(self.admin_f_23)
        self.admin_p_23.setObjectName(u"admin_p_23")
        sizePolicy1.setHeightForWidth(self.admin_p_23.sizePolicy().hasHeightForWidth())
        self.admin_p_23.setSizePolicy(sizePolicy1)
        self.admin_p_23.setStyleSheet(u"border-image: url(Images/image23.jpg);")

        self.verticalLayout_28.addWidget(self.admin_p_23)


        self.gridLayout_3.addWidget(self.admin_f_23, 4, 5, 1, 1)

        self.admin_f_19 = QFrame(self.admin_products_page)
        self.admin_f_19.setObjectName(u"admin_f_19")
        self.admin_f_19.setFrameShape(QFrame.StyledPanel)
        self.admin_f_19.setFrameShadow(QFrame.Raised)
        self.verticalLayout_24 = QVBoxLayout(self.admin_f_19)
        self.verticalLayout_24.setSpacing(6)
        self.verticalLayout_24.setContentsMargins(11, 11, 11, 11)
        self.verticalLayout_24.setObjectName(u"verticalLayout_24")
        self.admin_p_19 = QPushButton(self.admin_f_19)
        self.admin_p_19.setObjectName(u"admin_p_19")
        sizePolicy1.setHeightForWidth(self.admin_p_19.sizePolicy().hasHeightForWidth())
        self.admin_p_19.setSizePolicy(sizePolicy1)
        self.admin_p_19.setStyleSheet(u"border-image: url(Images/image19.jpg);")

        self.verticalLayout_24.addWidget(self.admin_p_19)


        self.gridLayout_3.addWidget(self.admin_f_19, 3, 6, 1, 1)

        self.admin_f_3 = QFrame(self.admin_products_page)
        self.admin_f_3.setObjectName(u"admin_f_3")
        self.admin_f_3.setFrameShape(QFrame.StyledPanel)
        self.admin_f_3.setFrameShadow(QFrame.Raised)
        self.verticalLayout_8 = QVBoxLayout(self.admin_f_3)
        self.verticalLayout_8.setSpacing(6)
        self.verticalLayout_8.setContentsMargins(11, 11, 11, 11)
        self.verticalLayout_8.setObjectName(u"verticalLayout_8")
        self.admin_p_3 = QPushButton(self.admin_f_3)
        self.admin_p_3.setObjectName(u"admin_p_3")
        sizePolicy1.setHeightForWidth(self.admin_p_3.sizePolicy().hasHeightForWidth())
        self.admin_p_3.setSizePolicy(sizePolicy1)
        self.admin_p_3.setStyleSheet(u"border-image: url(Images/image3.jpg);")

        self.verticalLayout_8.addWidget(self.admin_p_3)


        self.gridLayout_3.addWidget(self.admin_f_3, 0, 5, 1, 1)

        self.admin_f_13 = QFrame(self.admin_products_page)
        self.admin_f_13.setObjectName(u"admin_f_13")
        self.admin_f_13.setFrameShape(QFrame.StyledPanel)
        self.admin_f_13.setFrameShadow(QFrame.Raised)
        self.verticalLayout_18 = QVBoxLayout(self.admin_f_13)
        self.verticalLayout_18.setSpacing(6)
        self.verticalLayout_18.setContentsMargins(11, 11, 11, 11)
        self.verticalLayout_18.setObjectName(u"verticalLayout_18")
        self.admin_p_13 = QPushButton(self.admin_f_13)
        self.admin_p_13.setObjectName(u"admin_p_13")
        sizePolicy1.setHeightForWidth(self.admin_p_13.sizePolicy().hasHeightForWidth())
        self.admin_p_13.setSizePolicy(sizePolicy1)
        self.admin_p_13.setStyleSheet(u"border-image: url(Images/image13.jpg);")

        self.verticalLayout_18.addWidget(self.admin_p_13)


        self.gridLayout_3.addWidget(self.admin_f_13, 2, 5, 1, 1)

        self.admin_f_8 = QFrame(self.admin_products_page)
        self.admin_f_8.setObjectName(u"admin_f_8")
        self.admin_f_8.setFrameShape(QFrame.StyledPanel)
        self.admin_f_8.setFrameShadow(QFrame.Raised)
        self.verticalLayout_13 = QVBoxLayout(self.admin_f_8)
        self.verticalLayout_13.setSpacing(6)
        self.verticalLayout_13.setContentsMargins(11, 11, 11, 11)
        self.verticalLayout_13.setObjectName(u"verticalLayout_13")
        self.admin_p_8 = QPushButton(self.admin_f_8)
        self.admin_p_8.setObjectName(u"admin_p_8")
        sizePolicy1.setHeightForWidth(self.admin_p_8.sizePolicy().hasHeightForWidth())
        self.admin_p_8.setSizePolicy(sizePolicy1)
        self.admin_p_8.setStyleSheet(u"border-image: url(Images/image8.jpg);")

        self.verticalLayout_13.addWidget(self.admin_p_8)


        self.gridLayout_3.addWidget(self.admin_f_8, 1, 5, 1, 1)

        self.admin_f_20 = QFrame(self.admin_products_page)
        self.admin_f_20.setObjectName(u"admin_f_20")
        self.admin_f_20.setFrameShape(QFrame.StyledPanel)
        self.admin_f_20.setFrameShadow(QFrame.Raised)
        self.verticalLayout_25 = QVBoxLayout(self.admin_f_20)
        self.verticalLayout_25.setSpacing(6)
        self.verticalLayout_25.setContentsMargins(11, 11, 11, 11)
        self.verticalLayout_25.setObjectName(u"verticalLayout_25")
        self.admin_p_20 = QPushButton(self.admin_f_20)
        self.admin_p_20.setObjectName(u"admin_p_20")
        sizePolicy1.setHeightForWidth(self.admin_p_20.sizePolicy().hasHeightForWidth())
        self.admin_p_20.setSizePolicy(sizePolicy1)
        self.admin_p_20.setStyleSheet(u"border-image: url(Images/image20.jpg);")

        self.verticalLayout_25.addWidget(self.admin_p_20)


        self.gridLayout_3.addWidget(self.admin_f_20, 4, 1, 1, 1)

        self.admin_f_15 = QFrame(self.admin_products_page)
        self.admin_f_15.setObjectName(u"admin_f_15")
        self.admin_f_15.setFrameShape(QFrame.StyledPanel)
        self.admin_f_15.setFrameShadow(QFrame.Raised)
        self.verticalLayout_20 = QVBoxLayout(self.admin_f_15)
        self.verticalLayout_20.setSpacing(6)
        self.verticalLayout_20.setContentsMargins(11, 11, 11, 11)
        self.verticalLayout_20.setObjectName(u"verticalLayout_20")
        self.admin_p_15 = QPushButton(self.admin_f_15)
        self.admin_p_15.setObjectName(u"admin_p_15")
        sizePolicy1.setHeightForWidth(self.admin_p_15.sizePolicy().hasHeightForWidth())
        self.admin_p_15.setSizePolicy(sizePolicy1)
        self.admin_p_15.setStyleSheet(u"border-image: url(Images/image15.jpg);")

        self.verticalLayout_20.addWidget(self.admin_p_15)


        self.gridLayout_3.addWidget(self.admin_f_15, 3, 1, 1, 1)

        self.admin_f_1 = QFrame(self.admin_products_page)
        self.admin_f_1.setObjectName(u"admin_f_1")
        self.admin_f_1.setFrameShape(QFrame.StyledPanel)
        self.admin_f_1.setFrameShadow(QFrame.Raised)
        self.verticalLayout_6 = QVBoxLayout(self.admin_f_1)
        self.verticalLayout_6.setSpacing(6)
        self.verticalLayout_6.setContentsMargins(11, 11, 11, 11)
        self.verticalLayout_6.setObjectName(u"verticalLayout_6")
        self.admin_p_1 = QPushButton(self.admin_f_1)
        self.admin_p_1.setObjectName(u"admin_p_1")
        sizePolicy1.setHeightForWidth(self.admin_p_1.sizePolicy().hasHeightForWidth())
        self.admin_p_1.setSizePolicy(sizePolicy1)
        self.admin_p_1.setStyleSheet(u"border-image: url(Images/image1.jpg);")

        self.verticalLayout_6.addWidget(self.admin_p_1)


        self.gridLayout_3.addWidget(self.admin_f_1, 0, 3, 1, 1)

        self.admin_f_2 = QFrame(self.admin_products_page)
        self.admin_f_2.setObjectName(u"admin_f_2")
        self.admin_f_2.setFrameShape(QFrame.StyledPanel)
        self.admin_f_2.setFrameShadow(QFrame.Raised)
        self.verticalLayout_7 = QVBoxLayout(self.admin_f_2)
        self.verticalLayout_7.setSpacing(6)
        self.verticalLayout_7.setContentsMargins(11, 11, 11, 11)
        self.verticalLayout_7.setObjectName(u"verticalLayout_7")
        self.admin_p_2 = QPushButton(self.admin_f_2)
        self.admin_p_2.setObjectName(u"admin_p_2")
        sizePolicy1.setHeightForWidth(self.admin_p_2.sizePolicy().hasHeightForWidth())
        self.admin_p_2.setSizePolicy(sizePolicy1)
        self.admin_p_2.setStyleSheet(u"border-image: url(Images/image2.jpg);")

        self.verticalLayout_7.addWidget(self.admin_p_2)


        self.gridLayout_3.addWidget(self.admin_f_2, 0, 4, 1, 1)

        self.admin_f_5 = QFrame(self.admin_products_page)
        self.admin_f_5.setObjectName(u"admin_f_5")
        self.admin_f_5.setFrameShape(QFrame.StyledPanel)
        self.admin_f_5.setFrameShadow(QFrame.Raised)
        self.verticalLayout_10 = QVBoxLayout(self.admin_f_5)
        self.verticalLayout_10.setSpacing(6)
        self.verticalLayout_10.setContentsMargins(11, 11, 11, 11)
        self.verticalLayout_10.setObjectName(u"verticalLayout_10")
        self.admin_p_5 = QPushButton(self.admin_f_5)
        self.admin_p_5.setObjectName(u"admin_p_5")
        sizePolicy1.setHeightForWidth(self.admin_p_5.sizePolicy().hasHeightForWidth())
        self.admin_p_5.setSizePolicy(sizePolicy1)
        self.admin_p_5.setStyleSheet(u"border-image: url(Images/image5.jpg);")

        self.verticalLayout_10.addWidget(self.admin_p_5)


        self.gridLayout_3.addWidget(self.admin_f_5, 1, 1, 1, 1)

        self.admin_f_22 = QFrame(self.admin_products_page)
        self.admin_f_22.setObjectName(u"admin_f_22")
        self.admin_f_22.setFrameShape(QFrame.StyledPanel)
        self.admin_f_22.setFrameShadow(QFrame.Raised)
        self.verticalLayout_27 = QVBoxLayout(self.admin_f_22)
        self.verticalLayout_27.setSpacing(6)
        self.verticalLayout_27.setContentsMargins(11, 11, 11, 11)
        self.verticalLayout_27.setObjectName(u"verticalLayout_27")
        self.admin_p_22 = QPushButton(self.admin_f_22)
        self.admin_p_22.setObjectName(u"admin_p_22")
        sizePolicy1.setHeightForWidth(self.admin_p_22.sizePolicy().hasHeightForWidth())
        self.admin_p_22.setSizePolicy(sizePolicy1)
        self.admin_p_22.setStyleSheet(u"border-image: url(Images/image22.jpg);")

        self.verticalLayout_27.addWidget(self.admin_p_22)


        self.gridLayout_3.addWidget(self.admin_f_22, 4, 4, 1, 1)

        self.admin_f_11 = QFrame(self.admin_products_page)
        self.admin_f_11.setObjectName(u"admin_f_11")
        self.admin_f_11.setFrameShape(QFrame.StyledPanel)
        self.admin_f_11.setFrameShadow(QFrame.Raised)
        self.verticalLayout_16 = QVBoxLayout(self.admin_f_11)
        self.verticalLayout_16.setSpacing(6)
        self.verticalLayout_16.setContentsMargins(11, 11, 11, 11)
        self.verticalLayout_16.setObjectName(u"verticalLayout_16")
        self.admin_p_11 = QPushButton(self.admin_f_11)
        self.admin_p_11.setObjectName(u"admin_p_11")
        sizePolicy1.setHeightForWidth(self.admin_p_11.sizePolicy().hasHeightForWidth())
        self.admin_p_11.setSizePolicy(sizePolicy1)
        self.admin_p_11.setStyleSheet(u"border-image: url(Images/image11.jpg);")

        self.verticalLayout_16.addWidget(self.admin_p_11)


        self.gridLayout_3.addWidget(self.admin_f_11, 2, 3, 1, 1)

        self.admin_f_7 = QFrame(self.admin_products_page)
        self.admin_f_7.setObjectName(u"admin_f_7")
        self.admin_f_7.setFrameShape(QFrame.StyledPanel)
        self.admin_f_7.setFrameShadow(QFrame.Raised)
        self.verticalLayout_12 = QVBoxLayout(self.admin_f_7)
        self.verticalLayout_12.setSpacing(6)
        self.verticalLayout_12.setContentsMargins(11, 11, 11, 11)
        self.verticalLayout_12.setObjectName(u"verticalLayout_12")
        self.admin_p_7 = QPushButton(self.admin_f_7)
        self.admin_p_7.setObjectName(u"admin_p_7")
        sizePolicy1.setHeightForWidth(self.admin_p_7.sizePolicy().hasHeightForWidth())
        self.admin_p_7.setSizePolicy(sizePolicy1)
        self.admin_p_7.setStyleSheet(u"border-image: url(Images/image7.jpg);")

        self.verticalLayout_12.addWidget(self.admin_p_7)


        self.gridLayout_3.addWidget(self.admin_f_7, 1, 4, 1, 1)

        self.admin_f_6 = QFrame(self.admin_products_page)
        self.admin_f_6.setObjectName(u"admin_f_6")
        self.admin_f_6.setFrameShape(QFrame.StyledPanel)
        self.admin_f_6.setFrameShadow(QFrame.Raised)
        self.verticalLayout_11 = QVBoxLayout(self.admin_f_6)
        self.verticalLayout_11.setSpacing(6)
        self.verticalLayout_11.setContentsMargins(11, 11, 11, 11)
        self.verticalLayout_11.setObjectName(u"verticalLayout_11")
        self.admin_p_6 = QPushButton(self.admin_f_6)
        self.admin_p_6.setObjectName(u"admin_p_6")
        sizePolicy1.setHeightForWidth(self.admin_p_6.sizePolicy().hasHeightForWidth())
        self.admin_p_6.setSizePolicy(sizePolicy1)
        self.admin_p_6.setStyleSheet(u"border-image: url(Images/image6.jpg);")

        self.verticalLayout_11.addWidget(self.admin_p_6)


        self.gridLayout_3.addWidget(self.admin_f_6, 1, 3, 1, 1)

        self.admin_f_9 = QFrame(self.admin_products_page)
        self.admin_f_9.setObjectName(u"admin_f_9")
        self.admin_f_9.setFrameShape(QFrame.StyledPanel)
        self.admin_f_9.setFrameShadow(QFrame.Raised)
        self.verticalLayout_14 = QVBoxLayout(self.admin_f_9)
        self.verticalLayout_14.setSpacing(6)
        self.verticalLayout_14.setContentsMargins(11, 11, 11, 11)
        self.verticalLayout_14.setObjectName(u"verticalLayout_14")
        self.admin_p_9 = QPushButton(self.admin_f_9)
        self.admin_p_9.setObjectName(u"admin_p_9")
        sizePolicy1.setHeightForWidth(self.admin_p_9.sizePolicy().hasHeightForWidth())
        self.admin_p_9.setSizePolicy(sizePolicy1)
        self.admin_p_9.setStyleSheet(u"border-image: url(Images/image9.jpg);")

        self.verticalLayout_14.addWidget(self.admin_p_9)


        self.gridLayout_3.addWidget(self.admin_f_9, 1, 6, 1, 1)

        self.admin_f_10 = QFrame(self.admin_products_page)
        self.admin_f_10.setObjectName(u"admin_f_10")
        self.admin_f_10.setFrameShape(QFrame.StyledPanel)
        self.admin_f_10.setFrameShadow(QFrame.Raised)
        self.verticalLayout_15 = QVBoxLayout(self.admin_f_10)
        self.verticalLayout_15.setSpacing(6)
        self.verticalLayout_15.setContentsMargins(11, 11, 11, 11)
        self.verticalLayout_15.setObjectName(u"verticalLayout_15")
        self.admin_p_10 = QPushButton(self.admin_f_10)
        self.admin_p_10.setObjectName(u"admin_p_10")
        sizePolicy1.setHeightForWidth(self.admin_p_10.sizePolicy().hasHeightForWidth())
        self.admin_p_10.setSizePolicy(sizePolicy1)
        self.admin_p_10.setStyleSheet(u"border-image: url(Images/image10.jpg);")

        self.verticalLayout_15.addWidget(self.admin_p_10)


        self.gridLayout_3.addWidget(self.admin_f_10, 2, 1, 1, 1)

        self.admin_f_18 = QFrame(self.admin_products_page)
        self.admin_f_18.setObjectName(u"admin_f_18")
        self.admin_f_18.setFrameShape(QFrame.StyledPanel)
        self.admin_f_18.setFrameShadow(QFrame.Raised)
        self.verticalLayout_23 = QVBoxLayout(self.admin_f_18)
        self.verticalLayout_23.setSpacing(6)
        self.verticalLayout_23.setContentsMargins(11, 11, 11, 11)
        self.verticalLayout_23.setObjectName(u"verticalLayout_23")
        self.admin_p_18 = QPushButton(self.admin_f_18)
        self.admin_p_18.setObjectName(u"admin_p_18")
        sizePolicy1.setHeightForWidth(self.admin_p_18.sizePolicy().hasHeightForWidth())
        self.admin_p_18.setSizePolicy(sizePolicy1)
        self.admin_p_18.setStyleSheet(u"border-image: url(Images/image18.jpg);")

        self.verticalLayout_23.addWidget(self.admin_p_18)


        self.gridLayout_3.addWidget(self.admin_f_18, 3, 5, 1, 1)

        self.admin_f_12 = QFrame(self.admin_products_page)
        self.admin_f_12.setObjectName(u"admin_f_12")
        self.admin_f_12.setFrameShape(QFrame.StyledPanel)
        self.admin_f_12.setFrameShadow(QFrame.Raised)
        self.verticalLayout_17 = QVBoxLayout(self.admin_f_12)
        self.verticalLayout_17.setSpacing(6)
        self.verticalLayout_17.setContentsMargins(11, 11, 11, 11)
        self.verticalLayout_17.setObjectName(u"verticalLayout_17")
        self.admin_p_12 = QPushButton(self.admin_f_12)
        self.admin_p_12.setObjectName(u"admin_p_12")
        sizePolicy1.setHeightForWidth(self.admin_p_12.sizePolicy().hasHeightForWidth())
        self.admin_p_12.setSizePolicy(sizePolicy1)
        self.admin_p_12.setStyleSheet(u"border-image: url(Images/image12.jpg);")

        self.verticalLayout_17.addWidget(self.admin_p_12)


        self.gridLayout_3.addWidget(self.admin_f_12, 2, 4, 1, 1)

        self.admin_f_4 = QFrame(self.admin_products_page)
        self.admin_f_4.setObjectName(u"admin_f_4")
        self.admin_f_4.setFrameShape(QFrame.StyledPanel)
        self.admin_f_4.setFrameShadow(QFrame.Raised)
        self.verticalLayout_9 = QVBoxLayout(self.admin_f_4)
        self.verticalLayout_9.setSpacing(6)
        self.verticalLayout_9.setContentsMargins(11, 11, 11, 11)
        self.verticalLayout_9.setObjectName(u"verticalLayout_9")
        self.admin_p_4 = QPushButton(self.admin_f_4)
        self.admin_p_4.setObjectName(u"admin_p_4")
        sizePolicy1.setHeightForWidth(self.admin_p_4.sizePolicy().hasHeightForWidth())
        self.admin_p_4.setSizePolicy(sizePolicy1)
        self.admin_p_4.setStyleSheet(u"border-image: url(Images/image4.jpg);")

        self.verticalLayout_9.addWidget(self.admin_p_4)


        self.gridLayout_3.addWidget(self.admin_f_4, 0, 6, 1, 1)

        self.admin_f_17 = QFrame(self.admin_products_page)
        self.admin_f_17.setObjectName(u"admin_f_17")
        self.admin_f_17.setFrameShape(QFrame.StyledPanel)
        self.admin_f_17.setFrameShadow(QFrame.Raised)
        self.verticalLayout_22 = QVBoxLayout(self.admin_f_17)
        self.verticalLayout_22.setSpacing(6)
        self.verticalLayout_22.setContentsMargins(11, 11, 11, 11)
        self.verticalLayout_22.setObjectName(u"verticalLayout_22")
        self.admin_p_17 = QPushButton(self.admin_f_17)
        self.admin_p_17.setObjectName(u"admin_p_17")
        sizePolicy1.setHeightForWidth(self.admin_p_17.sizePolicy().hasHeightForWidth())
        self.admin_p_17.setSizePolicy(sizePolicy1)
        self.admin_p_17.setStyleSheet(u"border-image: url(Images/image17.jpg);")

        self.verticalLayout_22.addWidget(self.admin_p_17)


        self.gridLayout_3.addWidget(self.admin_f_17, 3, 4, 1, 1)

        self.admin_f_0 = QFrame(self.admin_products_page)
        self.admin_f_0.setObjectName(u"admin_f_0")
        self.admin_f_0.setFrameShape(QFrame.StyledPanel)
        self.admin_f_0.setFrameShadow(QFrame.Raised)
        self.verticalLayout_5 = QVBoxLayout(self.admin_f_0)
        self.verticalLayout_5.setSpacing(6)
        self.verticalLayout_5.setContentsMargins(11, 11, 11, 11)
        self.verticalLayout_5.setObjectName(u"verticalLayout_5")
        self.admin_p_0 = QPushButton(self.admin_f_0)
        self.admin_p_0.setObjectName(u"admin_p_0")
        sizePolicy1.setHeightForWidth(self.admin_p_0.sizePolicy().hasHeightForWidth())
        self.admin_p_0.setSizePolicy(sizePolicy1)
        self.admin_p_0.setAutoFillBackground(False)
        self.admin_p_0.setStyleSheet(u"border-image: url(Images/image0.jpg);")
        self.admin_p_0.setAutoDefault(False)
        self.admin_p_0.setFlat(False)

        self.verticalLayout_5.addWidget(self.admin_p_0)


        self.gridLayout_3.addWidget(self.admin_f_0, 0, 1, 1, 1)

        self.admin_f_16 = QFrame(self.admin_products_page)
        self.admin_f_16.setObjectName(u"admin_f_16")
        self.admin_f_16.setFrameShape(QFrame.StyledPanel)
        self.admin_f_16.setFrameShadow(QFrame.Raised)
        self.verticalLayout_21 = QVBoxLayout(self.admin_f_16)
        self.verticalLayout_21.setSpacing(6)
        self.verticalLayout_21.setContentsMargins(11, 11, 11, 11)
        self.verticalLayout_21.setObjectName(u"verticalLayout_21")
        self.admin_p_16 = QPushButton(self.admin_f_16)
        self.admin_p_16.setObjectName(u"admin_p_16")
        sizePolicy1.setHeightForWidth(self.admin_p_16.sizePolicy().hasHeightForWidth())
        self.admin_p_16.setSizePolicy(sizePolicy1)
        self.admin_p_16.setStyleSheet(u"border-image: url(Images/image16.jpg);")

        self.verticalLayout_21.addWidget(self.admin_p_16)


        self.gridLayout_3.addWidget(self.admin_f_16, 3, 3, 1, 1)

        self.admin_f_21 = QFrame(self.admin_products_page)
        self.admin_f_21.setObjectName(u"admin_f_21")
        self.admin_f_21.setFrameShape(QFrame.StyledPanel)
        self.admin_f_21.setFrameShadow(QFrame.Raised)
        self.verticalLayout_26 = QVBoxLayout(self.admin_f_21)
        self.verticalLayout_26.setSpacing(6)
        self.verticalLayout_26.setContentsMargins(11, 11, 11, 11)
        self.verticalLayout_26.setObjectName(u"verticalLayout_26")
        self.admin_p_21 = QPushButton(self.admin_f_21)
        self.admin_p_21.setObjectName(u"admin_p_21")
        sizePolicy1.setHeightForWidth(self.admin_p_21.sizePolicy().hasHeightForWidth())
        self.admin_p_21.setSizePolicy(sizePolicy1)
        self.admin_p_21.setStyleSheet(u"border-image: url(Images/image21.jpg);")

        self.verticalLayout_26.addWidget(self.admin_p_21)


        self.gridLayout_3.addWidget(self.admin_f_21, 4, 3, 1, 1)

        self.admin_f_24 = QFrame(self.admin_products_page)
        self.admin_f_24.setObjectName(u"admin_f_24")
        self.admin_f_24.setFrameShape(QFrame.StyledPanel)
        self.admin_f_24.setFrameShadow(QFrame.Raised)
        self.verticalLayout_29 = QVBoxLayout(self.admin_f_24)
        self.verticalLayout_29.setSpacing(6)
        self.verticalLayout_29.setContentsMargins(11, 11, 11, 11)
        self.verticalLayout_29.setObjectName(u"verticalLayout_29")
        self.admin_p_24 = QPushButton(self.admin_f_24)
        self.admin_p_24.setObjectName(u"admin_p_24")
        sizePolicy1.setHeightForWidth(self.admin_p_24.sizePolicy().hasHeightForWidth())
        self.admin_p_24.setSizePolicy(sizePolicy1)
        self.admin_p_24.setStyleSheet(u"border-image: url(Images/image24.jpg);")

        self.verticalLayout_29.addWidget(self.admin_p_24)


        self.gridLayout_3.addWidget(self.admin_f_24, 4, 6, 1, 1)

        self.admin_temp = QPushButton(self.admin_products_page)
        self.admin_temp.setObjectName(u"admin_temp")
        self.admin_temp.setStyleSheet(u"color: rgb(238, 238, 236);\n"
"background-color: rgb(32, 74, 135);")

        self.gridLayout_3.addWidget(self.admin_temp, 6, 1, 1, 1)

        self.admin_p1_logout = QPushButton(self.admin_products_page)
        self.admin_p1_logout.setObjectName(u"admin_p1_logout")
        self.admin_p1_logout.setStyleSheet(u"color: rgb(255, 255, 255);\n"
"background-color: rgb(164, 0, 0);")

        self.gridLayout_3.addWidget(self.admin_p1_logout, 6, 6, 1, 1)

        self.admin_tabs.addWidget(self.admin_products_page)
        self.admin_options_page = QWidget()
        self.admin_options_page.setObjectName(u"admin_options_page")
        self.gridLayout_5 = QGridLayout(self.admin_options_page)
        self.gridLayout_5.setSpacing(6)
        self.gridLayout_5.setContentsMargins(11, 11, 11, 11)
        self.gridLayout_5.setObjectName(u"gridLayout_5")
        self.admin_p2_return = QPushButton(self.admin_options_page)
        self.admin_p2_return.setObjectName(u"admin_p2_return")
        self.admin_p2_return.setStyleSheet(u"color: rgb(238, 238, 236);\n"
"background-color: rgb(32, 74, 135);")

        self.gridLayout_5.addWidget(self.admin_p2_return, 1, 0, 1, 1)

        self.d1_3 = QLabel(self.admin_options_page)
        self.d1_3.setObjectName(u"d1_3")

        self.gridLayout_5.addWidget(self.d1_3, 1, 1, 1, 1)

        self.admin_product_image = QLabel(self.admin_options_page)
        self.admin_product_image.setObjectName(u"admin_product_image")

        self.gridLayout_5.addWidget(self.admin_product_image, 0, 0, 1, 2)

        self.admin_options_frame = QFrame(self.admin_options_page)
        self.admin_options_frame.setObjectName(u"admin_options_frame")
        self.admin_options_frame.setFrameShape(QFrame.StyledPanel)
        self.admin_options_frame.setFrameShadow(QFrame.Raised)
        self.verticalLayout_4 = QVBoxLayout(self.admin_options_frame)
        self.verticalLayout_4.setSpacing(6)
        self.verticalLayout_4.setContentsMargins(11, 11, 11, 11)
        self.verticalLayout_4.setObjectName(u"verticalLayout_4")
        self.admin_charact_frame = QFrame(self.admin_options_frame)
        self.admin_charact_frame.setObjectName(u"admin_charact_frame")
        self.admin_charact_frame.setFrameShape(QFrame.StyledPanel)
        self.admin_charact_frame.setFrameShadow(QFrame.Raised)
        self.gridLayout_7 = QGridLayout(self.admin_charact_frame)
        self.gridLayout_7.setSpacing(6)
        self.gridLayout_7.setContentsMargins(11, 11, 11, 11)
        self.gridLayout_7.setObjectName(u"gridLayout_7")
        self.admin_current_price = QLabel(self.admin_charact_frame)
        self.admin_current_price.setObjectName(u"admin_current_price")
        sizePolicy1.setHeightForWidth(self.admin_current_price.sizePolicy().hasHeightForWidth())
        self.admin_current_price.setSizePolicy(sizePolicy1)

        self.gridLayout_7.addWidget(self.admin_current_price, 2, 0, 1, 2)

        self.admin_price_input = QLineEdit(self.admin_charact_frame)
        self.admin_price_input.setObjectName(u"admin_price_input")
        sizePolicy4 = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Fixed)
        sizePolicy4.setHorizontalStretch(0)
        sizePolicy4.setVerticalStretch(0)
        sizePolicy4.setHeightForWidth(self.admin_price_input.sizePolicy().hasHeightForWidth())
        self.admin_price_input.setSizePolicy(sizePolicy4)

        self.gridLayout_7.addWidget(self.admin_price_input, 2, 2, 1, 2)

        self.admin_name_input = QLineEdit(self.admin_charact_frame)
        self.admin_name_input.setObjectName(u"admin_name_input")
        sizePolicy4.setHeightForWidth(self.admin_name_input.sizePolicy().hasHeightForWidth())
        self.admin_name_input.setSizePolicy(sizePolicy4)

        self.gridLayout_7.addWidget(self.admin_name_input, 1, 2, 1, 2)

        self.admin_current_name = QLabel(self.admin_charact_frame)
        self.admin_current_name.setObjectName(u"admin_current_name")
        sizePolicy1.setHeightForWidth(self.admin_current_name.sizePolicy().hasHeightForWidth())
        self.admin_current_name.setSizePolicy(sizePolicy1)

        self.gridLayout_7.addWidget(self.admin_current_name, 1, 0, 1, 2)

        self.admin_p_apply_changes = QPushButton(self.admin_charact_frame)
        self.admin_p_apply_changes.setObjectName(u"admin_p_apply_changes")
        self.admin_p_apply_changes.setStyleSheet(u"color: rgb(238, 238, 236);\n"
"background-color: rgb(32, 74, 135);")

        self.gridLayout_7.addWidget(self.admin_p_apply_changes, 4, 1, 1, 2)

        self.current_charact_1 = QLabel(self.admin_charact_frame)
        self.current_charact_1.setObjectName(u"current_charact_1")
        sizePolicy2.setHeightForWidth(self.current_charact_1.sizePolicy().hasHeightForWidth())
        self.current_charact_1.setSizePolicy(sizePolicy2)
        self.current_charact_1.setAlignment(Qt.AlignCenter)

        self.gridLayout_7.addWidget(self.current_charact_1, 0, 2, 1, 2)

        self.current_charact_2 = QLabel(self.admin_charact_frame)
        self.current_charact_2.setObjectName(u"current_charact_2")
        sizePolicy2.setHeightForWidth(self.current_charact_2.sizePolicy().hasHeightForWidth())
        self.current_charact_2.setSizePolicy(sizePolicy2)
        self.current_charact_2.setAlignment(Qt.AlignCenter)

        self.gridLayout_7.addWidget(self.current_charact_2, 0, 0, 1, 2)

        self.admin_current_image = QLabel(self.admin_charact_frame)
        self.admin_current_image.setObjectName(u"admin_current_image")

        self.gridLayout_7.addWidget(self.admin_current_image, 3, 0, 1, 1)

        self.admin_p_selectImage = QPushButton(self.admin_charact_frame)
        self.admin_p_selectImage.setObjectName(u"admin_p_selectImage")
        self.admin_p_selectImage.setStyleSheet(u"color: rgb(238, 238, 236);\n"
"background-color: rgb(32, 74, 135);")

        self.gridLayout_7.addWidget(self.admin_p_selectImage, 3, 2, 1, 2)


        self.verticalLayout_4.addWidget(self.admin_charact_frame)

        self.admin_refill_frame = QFrame(self.admin_options_frame)
        self.admin_refill_frame.setObjectName(u"admin_refill_frame")
        self.admin_refill_frame.setFrameShape(QFrame.StyledPanel)
        self.admin_refill_frame.setFrameShadow(QFrame.Raised)
        self.gridLayout_8 = QGridLayout(self.admin_refill_frame)
        self.gridLayout_8.setSpacing(6)
        self.gridLayout_8.setContentsMargins(11, 11, 11, 11)
        self.gridLayout_8.setObjectName(u"gridLayout_8")
        self.refill_qty = QLabel(self.admin_refill_frame)
        self.refill_qty.setObjectName(u"refill_qty")
        self.refill_qty.setAlignment(Qt.AlignCenter)

        self.gridLayout_8.addWidget(self.refill_qty, 0, 2, 1, 1)

        self.current_units = QLabel(self.admin_refill_frame)
        self.current_units.setObjectName(u"current_units")
        self.current_units.setAlignment(Qt.AlignCenter)

        self.gridLayout_8.addWidget(self.current_units, 0, 0, 1, 1)

        self.admin_current_qty = QLabel(self.admin_refill_frame)
        self.admin_current_qty.setObjectName(u"admin_current_qty")
        self.admin_current_qty.setAlignment(Qt.AlignCenter)

        self.gridLayout_8.addWidget(self.admin_current_qty, 1, 0, 1, 1)

        self.admin_refill_spinBox = QSpinBox(self.admin_refill_frame)
        self.admin_refill_spinBox.setObjectName(u"admin_refill_spinBox")

        self.gridLayout_8.addWidget(self.admin_refill_spinBox, 1, 2, 1, 1)

        self.admin_refill_button = QPushButton(self.admin_refill_frame)
        self.admin_refill_button.setObjectName(u"admin_refill_button")
        self.admin_refill_button.setStyleSheet(u"color: rgb(238, 238, 236);\n"
"background-color: rgb(32, 74, 135);")

        self.gridLayout_8.addWidget(self.admin_refill_button, 4, 1, 1, 1)

        self.admin_of_message = QLabel(self.admin_refill_frame)
        self.admin_of_message.setObjectName(u"admin_of_message")

        self.gridLayout_8.addWidget(self.admin_of_message, 3, 2, 1, 1)


        self.verticalLayout_4.addWidget(self.admin_refill_frame)


        self.gridLayout_5.addWidget(self.admin_options_frame, 0, 3, 1, 3)

        self.admin_p2_logout = QPushButton(self.admin_options_page)
        self.admin_p2_logout.setObjectName(u"admin_p2_logout")
        self.admin_p2_logout.setStyleSheet(u"color: rgb(238, 238, 236);\n"
"background-color: rgb(164, 0, 0);")

        self.gridLayout_5.addWidget(self.admin_p2_logout, 1, 5, 1, 1)

        self.admin_tabs.addWidget(self.admin_options_page)
        self.admin_temp_page = QWidget()
        self.admin_temp_page.setObjectName(u"admin_temp_page")
        self.gridLayout_9 = QGridLayout(self.admin_temp_page)
        self.gridLayout_9.setSpacing(6)
        self.gridLayout_9.setContentsMargins(11, 11, 11, 11)
        self.gridLayout_9.setObjectName(u"gridLayout_9")
        self.temp_indications = QLabel(self.admin_temp_page)
        self.temp_indications.setObjectName(u"temp_indications")
        self.temp_indications.setAlignment(Qt.AlignCenter)

        self.gridLayout_9.addWidget(self.temp_indications, 0, 0, 1, 3)

        self.temp_max = QLabel(self.admin_temp_page)
        self.temp_max.setObjectName(u"temp_max")

        self.gridLayout_9.addWidget(self.temp_max, 1, 2, 1, 1)

        self.temp_lecture = QLabel(self.admin_temp_page)
        self.temp_lecture.setObjectName(u"temp_lecture")
        self.temp_lecture.setAlignment(Qt.AlignCenter)

        self.gridLayout_9.addWidget(self.temp_lecture, 2, 1, 1, 1)

        self.temp_slider = QSlider(self.admin_temp_page)
        self.temp_slider.setObjectName(u"temp_slider")
        self.temp_slider.setOrientation(Qt.Horizontal)

        self.gridLayout_9.addWidget(self.temp_slider, 1, 1, 1, 1)

        self.temp_back = QPushButton(self.admin_temp_page)
        self.temp_back.setObjectName(u"temp_back")
        self.temp_back.setStyleSheet(u"color: rgb(238, 238, 236);\n"
"background-color: rgb(32, 74, 135);")

        self.gridLayout_9.addWidget(self.temp_back, 5, 0, 1, 1)

        self.temp_logout = QPushButton(self.admin_temp_page)
        self.temp_logout.setObjectName(u"temp_logout")
        self.temp_logout.setStyleSheet(u"color: rgb(238, 238, 236);\n"
"background-color: rgb(164, 0, 0);")

        self.gridLayout_9.addWidget(self.temp_logout, 5, 2, 1, 1)

        self.temp_min = QLabel(self.admin_temp_page)
        self.temp_min.setObjectName(u"temp_min")
        self.temp_min.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout_9.addWidget(self.temp_min, 1, 0, 1, 1)

        self.temp_p = QPushButton(self.admin_temp_page)
        self.temp_p.setObjectName(u"temp_p")
        self.temp_p.setStyleSheet(u"color: rgb(238, 238, 236);\n"
"background-color: rgb(32, 74, 135);")

        self.gridLayout_9.addWidget(self.temp_p, 3, 1, 1, 1)

        self.d1_4 = QLabel(self.admin_temp_page)
        self.d1_4.setObjectName(u"d1_4")

        self.gridLayout_9.addWidget(self.d1_4, 4, 1, 1, 1)

        self.admin_tabs.addWidget(self.admin_temp_page)
        self.admin_img_page = QWidget()
        self.admin_img_page.setObjectName(u"admin_img_page")
        self.gridLayout_6 = QGridLayout(self.admin_img_page)
        self.gridLayout_6.setSpacing(6)
        self.gridLayout_6.setContentsMargins(11, 11, 11, 11)
        self.gridLayout_6.setObjectName(u"gridLayout_6")
        self.img_13 = QPushButton(self.admin_img_page)
        self.img_13.setObjectName(u"img_13")
        sizePolicy1.setHeightForWidth(self.img_13.sizePolicy().hasHeightForWidth())
        self.img_13.setSizePolicy(sizePolicy1)
        self.img_13.setStyleSheet(u"border-image: url(Ref_Images/image13.jpg);")

        self.gridLayout_6.addWidget(self.img_13, 3, 3, 1, 1)

        self.img_1 = QPushButton(self.admin_img_page)
        self.img_1.setObjectName(u"img_1")
        sizePolicy1.setHeightForWidth(self.img_1.sizePolicy().hasHeightForWidth())
        self.img_1.setSizePolicy(sizePolicy1)
        self.img_1.setStyleSheet(u"border-image: url(Ref_Images/image1.jpg);")

        self.gridLayout_6.addWidget(self.img_1, 1, 1, 1, 1)

        self.img_instructions = QLabel(self.admin_img_page)
        self.img_instructions.setObjectName(u"img_instructions")
        sizePolicy4.setHeightForWidth(self.img_instructions.sizePolicy().hasHeightForWidth())
        self.img_instructions.setSizePolicy(sizePolicy4)
        self.img_instructions.setStyleSheet(u"")
        self.img_instructions.setAlignment(Qt.AlignCenter)

        self.gridLayout_6.addWidget(self.img_instructions, 0, 0, 1, 5)

        self.img_6 = QPushButton(self.admin_img_page)
        self.img_6.setObjectName(u"img_6")
        sizePolicy1.setHeightForWidth(self.img_6.sizePolicy().hasHeightForWidth())
        self.img_6.setSizePolicy(sizePolicy1)
        self.img_6.setStyleSheet(u"border-image: url(Ref_Images/image6.jpg);")

        self.gridLayout_6.addWidget(self.img_6, 2, 1, 1, 1)

        self.img_4 = QPushButton(self.admin_img_page)
        self.img_4.setObjectName(u"img_4")
        sizePolicy1.setHeightForWidth(self.img_4.sizePolicy().hasHeightForWidth())
        self.img_4.setSizePolicy(sizePolicy1)
        self.img_4.setStyleSheet(u"border-image: url(Ref_Images/image4.jpg);")

        self.gridLayout_6.addWidget(self.img_4, 1, 4, 1, 1)

        self.img_8 = QPushButton(self.admin_img_page)
        self.img_8.setObjectName(u"img_8")
        sizePolicy1.setHeightForWidth(self.img_8.sizePolicy().hasHeightForWidth())
        self.img_8.setSizePolicy(sizePolicy1)
        self.img_8.setStyleSheet(u"border-image: url(Ref_Images/image8.jpg);")

        self.gridLayout_6.addWidget(self.img_8, 2, 3, 1, 1)

        self.img_7 = QPushButton(self.admin_img_page)
        self.img_7.setObjectName(u"img_7")
        sizePolicy1.setHeightForWidth(self.img_7.sizePolicy().hasHeightForWidth())
        self.img_7.setSizePolicy(sizePolicy1)
        self.img_7.setStyleSheet(u"border-image: url(Ref_Images/image7.jpg);")

        self.gridLayout_6.addWidget(self.img_7, 2, 2, 1, 1)

        self.img_20 = QPushButton(self.admin_img_page)
        self.img_20.setObjectName(u"img_20")
        sizePolicy1.setHeightForWidth(self.img_20.sizePolicy().hasHeightForWidth())
        self.img_20.setSizePolicy(sizePolicy1)
        self.img_20.setStyleSheet(u"border-image: url(Ref_Images/image20.jpg);")

        self.gridLayout_6.addWidget(self.img_20, 5, 0, 1, 1)

        self.img_5 = QPushButton(self.admin_img_page)
        self.img_5.setObjectName(u"img_5")
        sizePolicy1.setHeightForWidth(self.img_5.sizePolicy().hasHeightForWidth())
        self.img_5.setSizePolicy(sizePolicy1)
        self.img_5.setStyleSheet(u"border-image: url(Ref_Images/image5.jpg);")

        self.gridLayout_6.addWidget(self.img_5, 2, 0, 1, 1)

        self.img_15 = QPushButton(self.admin_img_page)
        self.img_15.setObjectName(u"img_15")
        sizePolicy1.setHeightForWidth(self.img_15.sizePolicy().hasHeightForWidth())
        self.img_15.setSizePolicy(sizePolicy1)
        self.img_15.setStyleSheet(u"border-image: url(Ref_Images/image15.jpg);")

        self.gridLayout_6.addWidget(self.img_15, 4, 0, 1, 1)

        self.img_10 = QPushButton(self.admin_img_page)
        self.img_10.setObjectName(u"img_10")
        sizePolicy1.setHeightForWidth(self.img_10.sizePolicy().hasHeightForWidth())
        self.img_10.setSizePolicy(sizePolicy1)
        self.img_10.setStyleSheet(u"border-image: url(Ref_Images/image10.jpg);")

        self.gridLayout_6.addWidget(self.img_10, 3, 0, 1, 1)

        self.img_11 = QPushButton(self.admin_img_page)
        self.img_11.setObjectName(u"img_11")
        sizePolicy1.setHeightForWidth(self.img_11.sizePolicy().hasHeightForWidth())
        self.img_11.setSizePolicy(sizePolicy1)
        self.img_11.setStyleSheet(u"border-image: url(Ref_Images/image11.jpg);")

        self.gridLayout_6.addWidget(self.img_11, 3, 1, 1, 1)

        self.img_9 = QPushButton(self.admin_img_page)
        self.img_9.setObjectName(u"img_9")
        sizePolicy1.setHeightForWidth(self.img_9.sizePolicy().hasHeightForWidth())
        self.img_9.setSizePolicy(sizePolicy1)
        self.img_9.setStyleSheet(u"border-image: url(Ref_Images/image9.jpg);")

        self.gridLayout_6.addWidget(self.img_9, 2, 4, 1, 1)

        self.img_22 = QPushButton(self.admin_img_page)
        self.img_22.setObjectName(u"img_22")
        sizePolicy1.setHeightForWidth(self.img_22.sizePolicy().hasHeightForWidth())
        self.img_22.setSizePolicy(sizePolicy1)
        self.img_22.setStyleSheet(u"border-image: url(Ref_Images/image22.jpg);")

        self.gridLayout_6.addWidget(self.img_22, 5, 2, 1, 1)

        self.img_2 = QPushButton(self.admin_img_page)
        self.img_2.setObjectName(u"img_2")
        sizePolicy1.setHeightForWidth(self.img_2.sizePolicy().hasHeightForWidth())
        self.img_2.setSizePolicy(sizePolicy1)
        self.img_2.setStyleSheet(u"border-image: url(Ref_Images/image2.jpg);")

        self.gridLayout_6.addWidget(self.img_2, 1, 2, 1, 1)

        self.img_3 = QPushButton(self.admin_img_page)
        self.img_3.setObjectName(u"img_3")
        sizePolicy1.setHeightForWidth(self.img_3.sizePolicy().hasHeightForWidth())
        self.img_3.setSizePolicy(sizePolicy1)
        self.img_3.setStyleSheet(u"border-image: url(Ref_Images/image3.jpg);")

        self.gridLayout_6.addWidget(self.img_3, 1, 3, 1, 1)

        self.img_24 = QPushButton(self.admin_img_page)
        self.img_24.setObjectName(u"img_24")
        sizePolicy1.setHeightForWidth(self.img_24.sizePolicy().hasHeightForWidth())
        self.img_24.setSizePolicy(sizePolicy1)
        self.img_24.setStyleSheet(u"border-image: url(Ref_Images/image24.jpg);")

        self.gridLayout_6.addWidget(self.img_24, 5, 4, 1, 1)

        self.img_21 = QPushButton(self.admin_img_page)
        self.img_21.setObjectName(u"img_21")
        sizePolicy1.setHeightForWidth(self.img_21.sizePolicy().hasHeightForWidth())
        self.img_21.setSizePolicy(sizePolicy1)
        self.img_21.setStyleSheet(u"border-image: url(Ref_Images/image21.jpg);")

        self.gridLayout_6.addWidget(self.img_21, 5, 1, 1, 1)

        self.img_23 = QPushButton(self.admin_img_page)
        self.img_23.setObjectName(u"img_23")
        sizePolicy1.setHeightForWidth(self.img_23.sizePolicy().hasHeightForWidth())
        self.img_23.setSizePolicy(sizePolicy1)
        self.img_23.setStyleSheet(u"border-image: url(Ref_Images/image23.jpg);")

        self.gridLayout_6.addWidget(self.img_23, 5, 3, 1, 1)

        self.img_14 = QPushButton(self.admin_img_page)
        self.img_14.setObjectName(u"img_14")
        sizePolicy1.setHeightForWidth(self.img_14.sizePolicy().hasHeightForWidth())
        self.img_14.setSizePolicy(sizePolicy1)
        self.img_14.setStyleSheet(u"border-image: url(Ref_Images/image14.jpg);")

        self.gridLayout_6.addWidget(self.img_14, 3, 4, 1, 1)

        self.img_0 = QPushButton(self.admin_img_page)
        self.img_0.setObjectName(u"img_0")
        sizePolicy1.setHeightForWidth(self.img_0.sizePolicy().hasHeightForWidth())
        self.img_0.setSizePolicy(sizePolicy1)
        self.img_0.setStyleSheet(u"border-image: url(Ref_Images/image0.jpg);")

        self.gridLayout_6.addWidget(self.img_0, 1, 0, 1, 1)

        self.img_17 = QPushButton(self.admin_img_page)
        self.img_17.setObjectName(u"img_17")
        sizePolicy1.setHeightForWidth(self.img_17.sizePolicy().hasHeightForWidth())
        self.img_17.setSizePolicy(sizePolicy1)
        self.img_17.setStyleSheet(u"border-image: url(Ref_Images/image17.jpg);")

        self.gridLayout_6.addWidget(self.img_17, 4, 2, 1, 1)

        self.img_19 = QPushButton(self.admin_img_page)
        self.img_19.setObjectName(u"img_19")
        sizePolicy1.setHeightForWidth(self.img_19.sizePolicy().hasHeightForWidth())
        self.img_19.setSizePolicy(sizePolicy1)
        self.img_19.setStyleSheet(u"border-image: url(Ref_Images/image19.jpg);")

        self.gridLayout_6.addWidget(self.img_19, 4, 4, 1, 1)

        self.img_12 = QPushButton(self.admin_img_page)
        self.img_12.setObjectName(u"img_12")
        sizePolicy1.setHeightForWidth(self.img_12.sizePolicy().hasHeightForWidth())
        self.img_12.setSizePolicy(sizePolicy1)
        self.img_12.setStyleSheet(u"border-image: url(Ref_Images/image12.jpg);")

        self.gridLayout_6.addWidget(self.img_12, 3, 2, 1, 1)

        self.img_16 = QPushButton(self.admin_img_page)
        self.img_16.setObjectName(u"img_16")
        sizePolicy1.setHeightForWidth(self.img_16.sizePolicy().hasHeightForWidth())
        self.img_16.setSizePolicy(sizePolicy1)
        self.img_16.setStyleSheet(u"border-image: url(Ref_Images/image16.jpg);")

        self.gridLayout_6.addWidget(self.img_16, 4, 1, 1, 1)

        self.img_18 = QPushButton(self.admin_img_page)
        self.img_18.setObjectName(u"img_18")
        sizePolicy1.setHeightForWidth(self.img_18.sizePolicy().hasHeightForWidth())
        self.img_18.setSizePolicy(sizePolicy1)
        self.img_18.setStyleSheet(u"border-image: url(Ref_Images/image18.jpg);")

        self.gridLayout_6.addWidget(self.img_18, 4, 3, 1, 1)

        self.admin_image_p_back = QPushButton(self.admin_img_page)
        self.admin_image_p_back.setObjectName(u"admin_image_p_back")
        self.admin_image_p_back.setStyleSheet(u"color: rgb(238, 238, 236);\n"
"background-color: rgb(32, 74, 135);")

        self.gridLayout_6.addWidget(self.admin_image_p_back, 6, 0, 1, 1)

        self.img_logout = QPushButton(self.admin_img_page)
        self.img_logout.setObjectName(u"img_logout")
        self.img_logout.setStyleSheet(u"color: rgb(238, 238, 236);\n"
"background-color: rgb(164, 0, 0);")

        self.gridLayout_6.addWidget(self.img_logout, 6, 4, 1, 1)

        self.admin_tabs.addWidget(self.admin_img_page)

        self.verticalLayout_3.addWidget(self.admin_tabs)

        self.general_tabs.addTab(self.Admin, "")

        self.verticalLayout_30.addWidget(self.general_tabs)

        self.temperature_label = QLabel(self.centralWidget)
        self.temperature_label.setObjectName(u"temperature_label")

        self.verticalLayout_30.addWidget(self.temperature_label)

        MainWindow.setCentralWidget(self.centralWidget)
        self.menuBar = QMenuBar(MainWindow)
        self.menuBar.setObjectName(u"menuBar")
        self.menuBar.setGeometry(QRect(0, 0, 479, 22))
        MainWindow.setMenuBar(self.menuBar)
        self.mainToolBar = QToolBar(MainWindow)
        self.mainToolBar.setObjectName(u"mainToolBar")
        MainWindow.addToolBar(Qt.TopToolBarArea, self.mainToolBar)
        self.statusBar = QStatusBar(MainWindow)
        self.statusBar.setObjectName(u"statusBar")
        MainWindow.setStatusBar(self.statusBar)

        self.retranslateUi(MainWindow)

        self.general_tabs.setCurrentIndex(1)
        self.user_tabs.setCurrentIndex(0)
        self.user_p_0.setDefault(False)
        self.admin_tabs.setCurrentIndex(4)
        self.admin_p_0.setDefault(False)


        QMetaObject.connectSlotsByName(MainWindow)
    # setupUi

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(QCoreApplication.translate("MainWindow", u"Vending Machine", None))
        self.user_p_16.setText("")
        self.user_p_13.setText("")
        self.user_p_9.setText("")
        self.user_p_2.setText("")
        self.user_p_21.setText("")
        self.user_p_15.setText("")
        self.user_p_3.setText("")
        self.user_p_19.setText("")
        self.user_p_0.setText("")
        self.user_p_20.setText("")
        self.user_p_14.setText("")
        self.user_p_4.setText("")
        self.user_p_17.setText("")
        self.user_p_10.setText("")
        self.user_p_12.setText("")
        self.user_p_7.setText("")
        self.user_p_23.setText("")
        self.user_p_8.setText("")
        self.user_p_11.setText("")
        self.user_p_5.setText("")
        self.user_p_24.setText("")
        self.user_p_1.setText("")
        self.user_p_18.setText("")
        self.user_p_6.setText("")
        self.user_p_22.setText("")
        self.user_image_label.setText("")
        self.user_price_label.setText(QCoreApplication.translate("MainWindow", u"<html><head/><body><p><span style=\" font-size:14pt;\">Precio: 600</span></p></body></html>", None))
        self.user_back_button.setText(QCoreApplication.translate("MainWindow", u"Back", None))
        self.user_buy_button.setText(QCoreApplication.translate("MainWindow", u"Buy", None))
        self.user_name_label.setText("")
        self.label.setText(QCoreApplication.translate("MainWindow", u"<html><head/><body><p align=\"center\"><span style=\" font-size:14pt;\">\u00a1Thanks, come back soon!</span></p></body></html>", None))
        self.general_tabs.setTabText(self.general_tabs.indexOf(self.User), QCoreApplication.translate("MainWindow", u"User", None))
        self.d5.setText("")
        self.admin_password_button.setText(QCoreApplication.translate("MainWindow", u"Send", None))
        self.d6.setText("")
        self.d1.setText(QCoreApplication.translate("MainWindow", u"Admin password:", None))
        self.admin_p_14.setText("")
        self.admin_p_23.setText("")
        self.admin_p_19.setText("")
        self.admin_p_3.setText("")
        self.admin_p_13.setText("")
        self.admin_p_8.setText("")
        self.admin_p_20.setText("")
        self.admin_p_15.setText("")
        self.admin_p_1.setText("")
        self.admin_p_2.setText("")
        self.admin_p_5.setText("")
        self.admin_p_22.setText("")
        self.admin_p_11.setText("")
        self.admin_p_7.setText("")
        self.admin_p_6.setText("")
        self.admin_p_9.setText("")
        self.admin_p_10.setText("")
        self.admin_p_18.setText("")
        self.admin_p_12.setText("")
        self.admin_p_4.setText("")
        self.admin_p_17.setText("")
        self.admin_p_0.setText("")
        self.admin_p_16.setText("")
        self.admin_p_21.setText("")
        self.admin_p_24.setText("")
        self.admin_temp.setText(QCoreApplication.translate("MainWindow", u"Temp", None))
        self.admin_p1_logout.setText(QCoreApplication.translate("MainWindow", u"Log Out", None))
        self.admin_p2_return.setText(QCoreApplication.translate("MainWindow", u"Back", None))
        self.admin_product_image.setText("")
        self.admin_current_price.setText(QCoreApplication.translate("MainWindow", u"TextLabel", None))
        self.admin_current_name.setText(QCoreApplication.translate("MainWindow", u"TextLabel", None))
        self.admin_p_apply_changes.setText(QCoreApplication.translate("MainWindow", u"Apply changes", None))
        self.current_charact_1.setText(QCoreApplication.translate("MainWindow", u"New", None))
        self.current_charact_2.setText(QCoreApplication.translate("MainWindow", u"Current", None))
        self.admin_current_image.setText(QCoreApplication.translate("MainWindow", u"TextLabel", None))
        self.admin_p_selectImage.setText(QCoreApplication.translate("MainWindow", u"Select Image", None))
        self.refill_qty.setText(QCoreApplication.translate("MainWindow", u"Refill quantity", None))
        self.current_units.setText(QCoreApplication.translate("MainWindow", u"Current units", None))
        self.admin_current_qty.setText(QCoreApplication.translate("MainWindow", u"TextLabel", None))
        self.admin_refill_button.setText(QCoreApplication.translate("MainWindow", u"Refill", None))
        self.admin_of_message.setText("")
        self.admin_p2_logout.setText(QCoreApplication.translate("MainWindow", u"Logout", None))
        self.temp_indications.setText(QCoreApplication.translate("MainWindow", u"Move the slider to acquire the desired temperature", None))
        self.temp_max.setText(QCoreApplication.translate("MainWindow", u"25 \u00b0C", None))
        self.temp_lecture.setText("")
        self.temp_back.setText(QCoreApplication.translate("MainWindow", u"Back", None))
        self.temp_logout.setText(QCoreApplication.translate("MainWindow", u"Logout", None))
        self.temp_min.setText(QCoreApplication.translate("MainWindow", u"4 \u00b0C", None))
        self.temp_p.setText(QCoreApplication.translate("MainWindow", u"Apply", None))
        self.img_instructions.setText(QCoreApplication.translate("MainWindow", u"Click the desired replacement image", None))
        self.admin_image_p_back.setText(QCoreApplication.translate("MainWindow", u"Back", None))
        self.img_logout.setText(QCoreApplication.translate("MainWindow", u"Logout", None))
        self.general_tabs.setTabText(self.general_tabs.indexOf(self.Admin), QCoreApplication.translate("MainWindow", u"Admin", None))
        self.temperature_label.setText(QCoreApplication.translate("MainWindow", u"TextLabel", None))
    # retranslateUi

