# IE-1119: Introduction to Embedded Systems
#
# Project 2: IoT Vending Machine
#
# Authors:
#   - Austin Blanco Solano
#   - Esteban Valverde Hernández
#
# Description: This module implements the GUI and networking for the central
# monitor system of the vending machine. 
# It receives messages (purchases, refills, empty stack) from the vending 
# machine through the MQTT protocol. Based on these messages it updates the 
# user interface. The GUI is a table containing each stack of the vending 
# machine, its product, row, column and amount of elementsin each stack.

from PySide2.QtWidgets import (
    QApplication, 
    QWidget,
    QVBoxLayout,
    QTableWidget, 
    QTableWidgetItem
)
from PySide2 import QtCore
import threading
from queue import Queue
from mqtt_client import Mqtt_client
import sys
import time

rows_dictionary = {'A':0, 'B':5, 'C':10, 'D':15, 'E':20}

class Inventory(QWidget):
    """Implements the GUI for the central monitor system"""
    def __init__(self):
        super(Inventory, self).__init__()

        self.tableWidget = QTableWidget()
        self.tableWidget.setRowCount(26)
        self.tableWidget.setColumnCount(5)
        self.setWindowTitle("Vending Machine Monitor System")

        self.tableWidget.setItem(0,0, QTableWidgetItem("Stack Number"))
        self.tableWidget.setItem(0,1, QTableWidgetItem("Product"))
        self.tableWidget.setItem(0,2, QTableWidgetItem("Row"))
        self.tableWidget.setItem(0,3, QTableWidgetItem("Column"))
        self.tableWidget.setItem(0,4, QTableWidgetItem("Amount"))

        self.refill("Chips Ahoy", "A", "1", "5")
        self.refill("Doritos", "A", "2", "5")
        self.refill("Snickers", "A", "3", "5")
        self.refill("Reeses", "A", "4", "5")
        self.refill("M&M", "A", "5", "5")

        self.refill("M&M Peanut", "B", "1", "5")
        self.refill("Maxim", "B", "2", "5")
        self.refill("Quest", "B", "3", "5")
        self.refill("Oreo", "B", "4", "5")
        self.refill("Oreo Cadbury", "B", "5", "5")

        self.refill("Coke", "C", "1", "5")
        self.refill("Pepsi", "C", "2", "5")
        self.refill("Rubicon", "C", "3", "5")
        self.refill("Ruffles", "C", "4", "5")
        self.refill("Skittles", "C", "5", "5")

        self.refill("Vinut", "D", "1", "5")
        self.refill("Twix", "D", "2", "5")
        self.refill("Gatorade", "D", "3", "5")
        self.refill("MTN Dew", "D", "4", "5")
        self.refill("Snapple", "D", "5", "5")

        self.refill("Hershey's", "E", "1", "5")
        self.refill("Kit-Kat", "E", "2", "5")
        self.refill("Lay's Spicy", "E", "3", "5")
        self.refill("Lays Masala", "E", "4", "5")
        self.refill("Pringles", "E", "5", "5")

        self.tableWidget.verticalHeader().setVisible(False)
        self.tableWidget.horizontalHeader().setVisible(False)

        for i in range(25):
            self.tableWidget.setItem(i+1,0, QTableWidgetItem(str(i+1)))
 
        self.vBoxLayout = QVBoxLayout()
        self.vBoxLayout.addWidget(self.tableWidget)
        self.setLayout(self.vBoxLayout)

    def insert(self, product, row, column, amount):
        """Inserts a new product according to row, column and amount information"""
        table_row = rows_dictionary[row] + int(column)
        self.tableWidget.setItem(table_row, 1, QTableWidgetItem(product))
        self.tableWidget.setItem(table_row, 2, QTableWidgetItem(row))
        self.tableWidget.setItem(table_row, 3, QTableWidgetItem(column))
        self.tableWidget.setItem(table_row, 4, QTableWidgetItem(amount))

        if amount == "0":
            self.tableWidget.item(table_row, 4).setBackground(QtCore.Qt.red)
    
    def purchase(self, product, row, column):
        """Update the monitor system based on a purchase"""
        table_row = rows_dictionary[row] + int(column)
        table_column = 4
        item = self.tableWidget.item(table_row, table_column)

        if item is not None:
            current_amount = item.text()
            new_amount = int(current_amount) - 1

            if new_amount < 0:
                new_amount = 0
            
            self.tableWidget.setItem(table_row, 
                                    table_column, 
                                    QTableWidgetItem(str(new_amount)))

    def refill(self, product, row, column, amount):
        """Update the monitor system based on a refill"""
        table_row = rows_dictionary[row] + int(column)
        table_column = 4
        item = self.tableWidget.item(table_row, table_column)
        
        if item is None:
            # If there is no item in the stack, insert it
            self.insert(product, row, column, amount)
        else:
            current_product = self.tableWidget.item(table_row, 1).text()
            current_amount = item.text()
            
            if int(amount) != 0:
                new_amount = int(current_amount) + int(amount)
            else:
                # Change of product in the stack
                new_amount = 0

            if new_amount <= 5:
                if current_product != product:
                    self.insert(product, row, column, amount)
                else:
                    # If the product is the same, just change the amount
                    self.tableWidget.setItem(table_row, 
                                            table_column, 
                                            QTableWidgetItem(str(new_amount)))
            else:
                # If the amount to be filled goes over 5 (the max), truncate at 5
                if current_product != product:
                    self.insert(product, row, column, "5")
                else:
                    self.tableWidget.setItem(table_row, 
                                            table_column, 
                                            QTableWidgetItem("5"))
        
    def needRefill(self, product, row, column):
        """Changes the color of the amount cell to red"""
        table_row = rows_dictionary[row] + int(column)
        table_column = 4    # Column corresponding to the amount

        item = self.tableWidget.item(table_row, table_column)

        if item.text() == "0":
            self.tableWidget.item(table_row, table_column).setBackground(QtCore.Qt.red)

class Worker(QtCore.QObject):
    """Contains the logic for the QThread"""
    def __init__(self, msg_queue,inventory):
        QtCore.QObject.__init__(self)
        
        self.msg_queue = msg_queue # GUI and MQTT client communication medium 
        self.inventory = inventory # GUI instance

        timeChanged = QtCore.Signal(object)

        self.timer = QtCore.QTimer(self) 
        self.timer.setInterval(500) # Every 500 ms go run main_process

        self.timer.timeout.connect(self.main_process)

    @QtCore.Slot()
    def start(self):
        """StartS the timer"""
        self.timer.start()

    @QtCore.Slot()
    def main_process(self):
        """Reads the messages from the MQTT server and updates the GUI"""
        # Every 500 ms check if the queue has messages
        if msg_queue.empty() != True:
            updates = str(self.msg_queue.get())
            updates_list = updates.split(",")

            # Get the transaction type: Refill, Purchase or NeedRefill
            tran_type = updates_list[0].replace("b'","")
            product = updates_list[1]
            row = updates_list[2]

            if tran_type == "Refill":
                amount = updates_list[4].replace("'","")
                column = updates_list[3]
                print("{},{},{},{},{}".format(tran_type, product, row, column,amount))
                self.inventory.refill(product, row, column, amount)
            elif tran_type == "Purchase":
                column = updates_list[3].replace("'","")
                print("{},{},{},{}".format(tran_type,product, row, column))
                self.inventory.purchase(product, row, column)
            elif tran_type == "NeedRefill":
                column = updates_list[3].replace("'","")
                print("{},{},{},{}".format(tran_type,product, row, column))
                self.inventory.needRefill(product, row, column)

        self.timer.setInterval(500) # Set the timer again

    def stop_timer(self):
        """Stops the timer"""
        self.timer.stop()

class WorkerThread(QtCore.QObject):
    """Moves Worker instance onto QThread"""
    def __init__(self, msg_queue, inventory):
        QtCore.QObject.__init__(self)

        self.emitter = Worker(msg_queue, inventory)
        self.thread = QtCore.QThread(self)
        self.emitter.moveToThread(self.thread) # Make the Woker run on the QThread

        self.thread.started.connect(self.emitter.start)
        self.thread.finished.connect(self.emitter.deleteLater)

    @QtCore.Slot()
    def start(self):
        """Starts the QThread"""
        self.thread.start()
    
    def stop(self):
        """Stops the QThread gracefully"""
        if self.thread.isRunning():
            self.thread.quit()
            self.thread.wait()
            print("Exit thread")

def runGUI(msg_queue):
    """To run on a thread to instantiate the monitor system GUI"""
    app = QApplication([])
    inventory = Inventory()
    emitter = WorkerThread(msg_queue, inventory)
    emitter.start()
    inventory.show()
    app.exec_()
    emitter.stop()

def runMqttClient(msg_queue):
    """To run on a thread to instantiate a MQTT client"""
    client = Mqtt_client(msg_queue)
    client.subscribe("vending_machine")
    client.loop_start()

if __name__ == "__main__":
    msg_queue = Queue() 
    t1 = threading.Thread(target=runGUI, args =(msg_queue, ))
    t2 = threading.Thread(target=runMqttClient, args =(msg_queue, ))
    t1.start()
    t2.start() 
