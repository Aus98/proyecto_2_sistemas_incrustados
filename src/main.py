# IE-1119: Introduction to Embedded Systems
#
# Project 2: IoT Vending Machine
#
# Authors:
#   - Austin Blanco Solano
#   - Esteban Valverde Hernández
#
# Description: This module implements the GUI and networking for the vending 
# machine. Includes the user mode interface to buy products and the adminis-
# tration mode to refill stacks. It sends messages (purchases, refills, empty 
# stack) through the MQTT protocol to message broker running Mosquitto v1.5.8 
# in a local machine.

import os
import sys
import time
import math
import signal
import hashlib
import threading
from numpy import interp
from queue import Queue
from shutil import copyfile
from PySide2.QtGui import *
from PySide2.QtCore import *
from PySide2.QtWidgets import *
from functools import partial
from mqtt_client import Mqtt_client
from ui_mainWindow import Ui_MainWindow
 
# Main window class handles the user interface
# widgets as imported from ui_mainWindow class
class MainWindow(QMainWindow, Ui_MainWindow):
    
    def __init__(self, msg_queue=None):
        self.ref_img_path = "./Ref_Images/"
        self.img_path = "./Images/"
        self.establishOrigImages()
        super(MainWindow, self).__init__()
        self.setupUi(self)

        # Initial values
        self.password_hash = "4dfe26927473f29a2e4a45b8b8d52482128c6d4f0d1634cb5be3e414"
        self.salt = "IE119_Embedded_Systems"
        self.prices = [500,800,1000,400,750,
                       800,600,650,500,800,
                       600,600,950,600,600,
                       700,500,1000,1200,1300,
                       600,500,700,700,650]
        self.names = ["Chips Ahoy","Doritos","Snickers","Reeses","M&M",
                    "M&M Peanut","Maxim","Quest","Oreo","Oreo Cadbury",
                    "Coke","Pepsi","Rubicon","Ruffles","Skittles",
                    "Vinut","Twix","Gatorade","MTN Dew","Snapple",
                    "Hershey's","Kit-Kat","Lay's Spicy","Lays Masala","Pringles"]
        self.quantities = [5]*25 # All products start full with 5 units
        self.userCurrentProduct = 0 # Default user product index
        self.adminCurrentProduct = 0 # Default admin product index
        self.currentImage = -1 # -1 Means no image selected, otherwise uses image index
        self.temperature = 4 # Initial default temperature
        self.msg_queue = msg_queue # Assign queue element
        
        # Default pages and tab
        self.admin_tabs.setCurrentIndex(0) # Assign admin default page
        self.user_tabs.setCurrentIndex(0) # Assign user default page
        self.general_tabs.setCurrentIndex(0) # Assign application default tab

        # Text formatting
        self.user_price_label.setStyleSheet("font-size:14pt;")
        self.user_name_label.setStyleSheet("font-size:12pt;")
        self.temp_lecture.setStyleSheet("font-size:18pt;")

        # Init functions
        self.tempUpdate()
        self.assignWidgets()
        self.show()

    # Widgets are assigned to each corresponding event
    def assignWidgets(self):
        # Connect button actions
        for i in range(25):
            eval("self.user_p_"+str(i)+".clicked.connect(partial(self.userChosenProduct,i))")
            eval("self.admin_p_"+str(i)+".clicked.connect(partial(self.adminChosenProduct,i))")
            eval("self.img_"+str(i)+".clicked.connect(partial(self.imageChosen,i))")
        self.user_back_button.clicked.connect(self.userBack)
        self.user_buy_button.clicked.connect(self.userBuy)
        self.admin_password_button.clicked.connect(self.checkPassword)
        self.admin_p1_logout.clicked.connect(self.adminLogout)
        self.admin_p2_logout.clicked.connect(self.adminLogout)
        self.admin_p2_return.clicked.connect(self.adminBack)
        self.admin_p_apply_changes.clicked.connect(self.adminChanges)
        self.admin_refill_button.clicked.connect(self.adminRefill)
        self.admin_refill_spinBox.valueChanged.connect(self.checkRefillValue)
        self.general_tabs.currentChanged.connect(self.updateQuantity)
        self.temp_back.clicked.connect(self.adminBack)
        self.temp_logout.clicked.connect(self.adminLogout)
        self.admin_temp.clicked.connect(self.tempChosen)
        self.temp_slider.valueChanged.connect(self.sliderCalc)
        self.temp_p.clicked.connect(self.tempUpdate)
        self.admin_image_p_back.clicked.connect(self.imageBack)
        self.img_logout.clicked.connect(self.adminLogout)
        self.admin_p_selectImage.clicked.connect(self.imageSelection)

    # Obtain row [A..E], column [1..5] format from the working indexes
    def rowColFromIndex(self,index):
        divison = math.floor(index/5)
        switcher = {
            0: "A",
            1: "B",
            2: "C",
            3: "D",
            4: "E"
        }
        row = switcher.get(divison,"")
        col = index%5+1
        return [str(row),str(col)]

    # Initially establish default images on the Images directory
    # using Ref_Images as the default images
    def establishOrigImages(self):
        for i in range(25):
            current=self.img_path+"image"+str(i)+".jpg"
            reference=self.ref_img_path+"image"+str(i)+".jpg"
            os.remove(current)
            copyfile(reference,current)

    # Move user back to the product selection page
    def userBack(self):
        self.user_tabs.setCurrentIndex(0)

    # Update details according to the product bought
    def userBuy(self):
        # Dispense from lowest count machine
        productName = self.names[self.userCurrentProduct]
        leastQuantity = self.quantities[self.userCurrentProduct]
        bestIndex = self.userCurrentProduct

        for i in range(25):
            quantity = self.quantities[i]
            if(self.names[i]==productName and quantity!=0 and quantity<leastQuantity):
                bestIndex=i
                leastQuantity = quantity

        # Update quantity
        self.quantities[bestIndex]-=1

        # Send purchase message
        if self.msg_queue is not None: 
            product = self.rowColFromIndex(bestIndex)
            self.msg_queue.put("Purchase,{},{},{}".format(
                                self.names[bestIndex], product[0], product[1]))

        if self.quantities[bestIndex]==0:
            # Make empty products invisible to the user
            eval("self.user_p_"+str(bestIndex)+".setVisible(False)")
            
            # Send refill message
            if self.msg_queue is not None:
                product = self.rowColFromIndex(bestIndex)
                self.msg_queue.put("NeedRefill,{},{},{}".format(
                                    self.names[bestIndex], 
                                    product[0], product[1]))
        
        # Show thanks message for 2 s, then go back to product selection page
        self.user_tabs.setCurrentIndex(2)
        QTimer.singleShot(2000, self.userBack)

    # Move to product details page and update details accordingly
    def userChosenProduct(self, index):
        self.userCurrentProduct = index
        self.user_tabs.setCurrentIndex(1)
        self.user_image_label.setStyleSheet(u"image: url(Images/image"+str(index)+".jpg);")
        self.user_price_label.setText("Price: "+str(self.prices[index])+" ₡")
        self.user_name_label.setText("Name: "+str(self.names[index]))
        
    # Show red border for empty products in administration settings
    def borderColor(self):
        for i in range(25):
            if(self.quantities[i]==0):
                empty_styling = "background-color: rgb(164, 0, 0)"
                eval("self.admin_f_"+str(i)+".setStyleSheet(empty_styling)")

    # Password verification with salted sha3
    def checkPassword(self):
        input_password = self.admin_password_text.text()
        hashed=hashlib.sha3_224((self.salt+input_password).encode('utf-8')).hexdigest()
        if hashed == self.password_hash:
            self.admin_tabs.setCurrentIndex(1)
            # Update borders for the admin to identify empty products
            self.borderColor()
        else:
            self.admin_error_message.setText("Incorrect password")
        
    # Logout admin from administrator settings and move back to product
    # selection page, while protecting administrator setting with password
    def adminLogout(self):
        self.admin_password_text.setText("")
        self.admin_error_message.setText("")
        self.admin_tabs.setCurrentIndex(0)
        self.general_tabs.setCurrentIndex(0)

    # Move admin back to product selection settings page
    def adminBack(self):
        self.admin_tabs.setCurrentIndex(1)

    # Move back to product selection settings page from 
    # image change page
    def imageBack(self):
        self.admin_tabs.setCurrentIndex(2)
    
    # Move to image change page
    def imageSelection(self):
        self.admin_tabs.setCurrentIndex(4)

    # Update details according to the chosen replacement image
    def imageChosen(self,index):
        self.admin_tabs.setCurrentIndex(2)
        self.admin_current_image.setText("Image: img_"+str(index)+".jpg")
        self.currentImage = index

    # Update product description in admin settings for the specific product
    def updateDescription(self,index):
        self.admin_current_name.setText("Name: "+self.names[index])
        self.admin_current_price.setText("Price: "+str(self.prices[index])+" ₡")
        self.admin_name_input.setText("")
        self.admin_price_input.setText("")

    # Update product quantity with color changes for empty or full conditions
    def updateQuantity(self,index):
        self.admin_current_qty.setText(str(self.quantities[index]))
        self.admin_current_qty.setStyleSheet("color:black")
        self.admin_of_message.setText("")
        if(self.quantities[index]==0):
            self.admin_current_qty.setStyleSheet("color:red")
        if(self.quantities[index]==5):
            self.admin_of_message.setText("Product full")
            self.admin_of_message.setStyleSheet("color:red")
        # Update max refill value limit
        self.admin_refill_spinBox.setMaximum(5-self.quantities[self.adminCurrentProduct])

    # Update admin change settings page for the product selected
    def adminChosenProduct(self, index):
        self.adminCurrentProduct = index
        self.admin_tabs.setCurrentIndex(2)
        self.currentImage = -1
        self.admin_current_image.setText("")
        self.admin_product_image.setStyleSheet(u"image: url(Images/image"+str(index)+".jpg);")
        self.updateDescription(index)
        self.updateQuantity(index)
    
    # Perform admin changes based on input data
    def adminChanges(self):
        # Obtain current input data and update
        new_name=self.admin_name_input.text()
        new_price=self.admin_price_input.text()
        new_image=self.admin_current_image.text()
        if(new_name!=""):
            self.names[self.adminCurrentProduct]=new_name
        
        if(new_price!="" and new_price.isdecimal()):
            self.prices[self.adminCurrentProduct]=new_price
        
        if(self.currentImage!=-1):
            # If new image new product is implied, must be manually 
            # refilled after image changed
            self.quantities[self.adminCurrentProduct]=0
            # Make unselectable for user until refilled
            eval("self.user_p_"+str(self.adminCurrentProduct)+".setVisible(False)")
            self.borderColor()
            
            # Set details according to the corresponding image if 
            # not introduced by the user
            if(new_price==""):
                self.prices[self.adminCurrentProduct]=self.prices[self.currentImage]
            if(new_name==""):
                self.names[self.adminCurrentProduct]=self.names[self.currentImage]

            # Update current working image in Images directory
            current=self.img_path+"image"+str(self.adminCurrentProduct)+".jpg"
            reference=self.ref_img_path+"image"+str(self.currentImage)+".jpg"
            os.remove(current)
            copyfile(reference,current)
            style = "\"border-image: url("+current+")\""
            eval("self.user_p_"+str(self.adminCurrentProduct)+".setStyleSheet("+style+")")
            eval("self.admin_p_"+str(self.adminCurrentProduct)+".setStyleSheet("+style+")")
            # Update details of the product
            self.adminChosenProduct(self.adminCurrentProduct)

            # Send refill message as the new product will be empty
            product = self.rowColFromIndex(self.adminCurrentProduct)
            self.msg_queue.put("Refill,{},{},{},{}".format(
                                self.names[self.adminCurrentProduct],
                                product[0],product[1],
                                self.quantities[self.adminCurrentProduct]))

        # Clear image selected text
        self.admin_current_image.setText("")
        # Update description and image according to the selected details
        self.updateDescription(self.adminCurrentProduct)

    # Clean admin messages for refill max value indications
    def cleanOfMessage(self):
        self.admin_of_message.setText("")
        self.updateQuantity(self.adminCurrentProduct)

    # Update max refill indication
    def checkRefillValue(self):
        refill_qty = self.admin_refill_spinBox.value()
        if(self.quantities[self.adminCurrentProduct]+refill_qty==5):
            self.admin_of_message.setText("Max refill")
            self.admin_of_message.setStyleSheet("color:red")
            QTimer.singleShot(2000, self.cleanOfMessage)

    # Perform refill on selected product
    def adminRefill(self):
        refill_qty = self.admin_refill_spinBox.value()
        if(refill_qty>0):
            self.quantities[self.adminCurrentProduct]+=refill_qty
            # Set product visible for user and update admin settings border
            # to non visible, as the product is no longer empty
            not_empty_styling = "background-color: rgb(255,255,255)"
            eval("self.admin_f_"+str(self.adminCurrentProduct)+".setStyleSheet(not_empty_styling)")
            eval("self.user_p_"+str(self.adminCurrentProduct)+".setVisible(True)")
            self.updateQuantity(self.adminCurrentProduct)

            # Send refill message
            product = self.rowColFromIndex(self.adminCurrentProduct)
            self.msg_queue.put("Refill,{},{},{},{}".format(
                                self.names[self.adminCurrentProduct],
                                product[0],product[1],refill_qty))
    
    # Move to temperature change page
    def tempChosen(self):
        self.admin_tabs.setCurrentIndex(3)

    # Calculate temperature slider value
    def sliderCalc(self):
        slider_value=self.temp_slider.value()
        # Map value range from slider to desired temperature range
        adjusted_value=int(round(interp(slider_value,[1,99],[4,25])))
        # Show temperature estimated from slider
        self.temp_lecture.setText(str(adjusted_value)+" °C")
        return adjusted_value  

    # Update temperature to current slider value
    def tempUpdate(self):
        self.temperature = self.sliderCalc()
        self.temperature_label.setText("Temperature: "+str(self.temperature)+" °C")

# Gui operation main function
def runGUI(msg_queue):
    app = QApplication(sys.argv)
    mainWin = MainWindow(msg_queue)
    ret = app.exec_()
    sys.exit( ret )

# Mqtt client operation main function
def runMqttClient(msg_queue, stop):
    client = Mqtt_client(msg_queue)
    client.loop_start()
    i = 0
    topic =  "vending_machine"

    while True:
        if stop():
            # Control the end of the thread through a flag
            client.disconnect()
            break
        time.sleep(0.5) # Check message queue every 500 ms
        if msg_queue.qsize() != 0:
            updates = msg_queue.get()
            updates_list = updates.split(",")

            tran_type = updates_list[0].replace(",","")
            
            if tran_type == "Refill":
                print("refill " + updates)
                client.publish(topic, updates)
            elif tran_type == "Purchase":
                print("purchase " + updates)  
                client.publish(topic, updates)
            else:
                print("need refill " + updates)
                client.publish(topic, updates)


if __name__ == '__main__':
    # Use threads for concurrent operation
    msg_queue = Queue()
    stop_threads = False
    t1 = threading.Thread(target=runGUI, args =(msg_queue, ))
    t2 = threading.Thread(target=runMqttClient, args =(msg_queue, lambda: stop_threads))
    t1.start()
    t2.start()

    # Handle the end of the MQTT client thread based on the termination of the
    # GUI thread.
    limit = 3
    while threading.active_count() >= limit:
        if threading.active_count() == 4:
            limit = 4
    
    stop_threads = True
