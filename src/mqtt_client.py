# IE-1119: Introduction to Embedded Systems
#
# Project 2: IoT Vending Machine
#
# Authors:
#   - Austin Blanco Solano
#   - Esteban Valverde Hernández
#
# Description: This module implements a wrapper for the mqtt client from the
# paho-mqtt python package. It adds methods and callbacks for the client.

import paho.mqtt.client as mqtt

class Mqtt_client():
    """This class implements method and callbacks for an MQTT client"""

    def __init__(self, msg_queue=None, broker_address="127.0.0.1", port=1883):
        self.client = mqtt.Client()
        self.client.on_connect = self.on_connect
        self.client.on_message = self.on_message
        self.msg_queue = msg_queue
        self.client.connect(broker_address, port)

    def on_connect(self, client, userdata, flags, rc):
        """Callback for when the client receives a CONNACK from the server"""
        print("Connected with result code "+str(rc))

    def on_message(self, client, userdata, msg):
        """Callback for when the client receives a PUBLISH from the server"""
        print(msg.topic+" "+str(msg.payload))
        if self.msg_queue is not None:
            # Communicates the message through a queue
            self.msg_queue.put(msg.payload)

    def on_disconnect(self, client, userdata, rc):
        """Callback for when the client disconnects from the server"""
        self.client.loop_stop(force=False)

    def disconnect(self):
        """Gracefully disconnects the client from the server"""
        self.client.disconnect()

    def loop_forever(self):
        """Blocking form of the network loop"""
        self.client.loop_forever()
    
    def loop_start(self):
        """Call regularly to process network events"""
        self.client.loop_start()
    
    def publish(self, topic, msg_payload):
        """Publish a mesasge on the corresponding topic"""
        self.client.publish(topic, msg_payload)

    def subscribe(self, topic):
        """Subscribes to the corresponding topic"""
        self.client.subscribe(topic)

        