# Project 2 IE-1119: Introduction to Embedded Systems

## Description
This project corresponds to a practical implementation of an embedded IoT device, particularly a connected vending machine. The user interface was designed with QT Designer and the programming was made with PySide2. The communication with the monitor system was done with mqtt protocol. This prototype aims to provide the functionality of a vending machine that reports product purchases and refill necessities to a centralized machine.

## Authors
* Austin Blanco Solano
* Esteban Valverde Hernández

## Requirements
The programming language of choice is python.
* python >= 3.0

The following python packages are needed:
* numpy == 1.18.1
* paho_mqtt == 1.5.0
* PySide2 == 5.15.0
* Qt >= 4

In order to install them, you can use the python installer and the requirements.txt file.
* Install the packages
 ```sh 
  pip install -r requirements.txt
 ```

Mosquitto v1.5.8 was used as the MQTT message broker. In order to run the whole program, the mosquitto broker must be running before hand.

## Usage
The **src** directory contains the source files and the following commands are valid inside that directory only.
* Run GUI
    ```sh
    python3 main.py
    ```
* Run System Monitor
  ```sh 
  python3 monitor_system.py
    ```
## User Interface
The two main components of the prototype's UI correspond to the application gui and the system monitor gui, as described next.
### Application
The application presents the clickable products for the user selection, as shown in the image below.


* ![](src/Gui_examples/gui.png)

this products can change price, name and image based on the administrator changes, made through the admin tab. This admin tab is protected with a password. The admin list of possible changes include:
* Product's name.
* Product's price.
* Product's image.
* Product's quantity through refill.
* Camera temperature.

The camera temperature is shown in both the admin and user tabs and can be changed through the admin tab. 

Only the available products will be shown to the user in the product selection page, if a product is empty the admin interface will mark the border of the image as **red** for easier recognition of prioritary actions.

* Admin usage suppositions:
    * When the image of a product is changed, the corresponding product will be empty, until refilled by the admin, as a new product can't already have a certain amount.
    * It's the administrator responsibility to properly type the product's name and make any corresponding image and price changes, **if only the image is changed the values (price and name) for the product with that image will be used as default**, unless otherwise stated by the user.
    * The admin interface is set to be used while no purchases are being made, the interface will update properly, but is not the intended use.


### System Monitor
The system monitor updates the information of amount and name of a particular product, according to the mqtt communication, the user interface is shown in the image below.
* ![](src/Gui_examples/monitor.png)
